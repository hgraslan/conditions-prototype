#include <algorithm>

#include "ConditionAlgBase.hpp"


ConditionAlgBase::ConditionAlgBase( ConditionSvc          &  conditionService,
                                    framework::IScheduler & scheduler )
   : m_conditionService( conditionService )
{
   scheduler.registerConditionAlg( *this );
}


bool
ConditionAlgBase::inputsPresent( const ConditionSlot & slot ) const
{
   return std::all_of(
      m_inputHandles.cbegin(),
      m_inputHandles.cend(),
      [&slot]( const detail::AnyConditionReadHandle & handle ) -> bool {
         return handle.hasValue( slot );
      }
   );
}


bool
ConditionAlgBase::outputMissing( const ConditionSlot & slot ) const
{
   return std::any_of(
      m_outputHandles.cbegin(),
      m_outputHandles.cend(),
      [&slot]( const detail::AnyConditionWriteHandle & handle ) -> bool {
         return !handle.hasValue( slot );
      }
   );
}


#ifdef ALLOW_IO_IN_ALGORITHMS

bool
ConditionAlgBase::performsConditionIO() const
{
   return m_performsConditionIO;
}

#endif
