#ifndef CONDITION_ALG_BASE_HPP
#define CONDITION_ALG_BASE_HPP

#include "ConditionHandle.hpp"
#include "ConditionKind.hpp"
#include "ConditionSlot.hpp"

#include "detail/AnyConditionHandle.hpp"

#include "framework/IConditionAlg.hpp"
#include "framework/identifiers.hpp"
#include "framework/IScheduler.hpp"


// Forward declaration of other prototype components
class ConditionSvc;


// This class is intended to serve as a base class to all concrete condition algorithms. It provides them with an easy
// way to register input and output handles, and implements the dataflow management part of IConditionAlg, only leaving
// the contents of the execute() method up to the concrete implementation.
class ConditionAlgBase : public framework::IConditionAlg
{
public:

   // ConditionAlgs must have access to the ConditionSvc and register to the Scheduler during initialization
   ConditionAlgBase( ConditionSvc          & conditionService,
                     framework::IScheduler & scheduler );

   // Implement the dataflow management part of IConditionAlg
   bool inputsPresent( const ConditionSlot & slot ) const final override;
   bool outputMissing( const ConditionSlot & slot ) const final override;
#ifdef ALLOW_IO_IN_ALGORITHMS
   bool performsConditionIO() const final override;
#endif


protected:

   // Register a condition input
   template< typename T >
   const ConditionReadHandle<T> & registerInput( const framework::ConditionID & inputID );

#ifdef ALLOW_IO_IN_ALGORITHMS
   // Register a condition output (an algorithm may have raw outputs if it is allowed to carry out IO)
   template< typename T >
   const ConditionWriteHandle<T> & registerOutput( const framework::ConditionID & outputID,
                                                   const ConditionKind            outputKind );
#else
   // Register a derived condition output (only option if IO is not allowed)
   template< typename T >
   const ConditionWriteHandle<T> & registerOutput( const framework::ConditionID & outputID );
#endif


private:

   // Commonalities between the two versions of registerOutput go here
   template< typename T >
   const ConditionWriteHandle<T> & registerOutputImpl( const framework::ConditionID & outputID,
                                                       const ConditionKind            outputKind );

   // Keep track of the location of the condition service
   const ConditionSvc & m_conditionService;

   // Keep track of our inputs and outputs
   std::vector< detail::AnyConditionReadHandle > m_inputHandles;
   std::vector< detail::AnyConditionWriteHandle > m_outputHandles;

#ifdef ALLOW_IO_IN_ALGORITHMS
   // If this is allowed, track whether the algorithm may carry out condition IO and produce raw conditions
   bool m_performsConditionIO = false;
#endif

};


// Inline implementations
#include "ConditionAlgBase.ipp"

#endif
