#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"


template< typename T >
const ConditionReadHandle<T> &
ConditionAlgBase::registerInput( const framework::ConditionID & inputID )
{
   // Get a read handle to our input and put it in m_inputHandles
   m_inputHandles.emplace_back(
      m_conditionService.transientStore().registerInput<T>(
         reinterpret_cast<framework::ConditionUserID>( this ),
         inputID
      )
   );

   // Provide a reference to the input to the client
   return m_inputHandles.back().get<T>();
}


#ifdef ALLOW_IO_IN_ALGORITHMS

template< typename T >
const ConditionWriteHandle<T> &
ConditionAlgBase::registerOutput( const framework::ConditionID & outputID,
                                  const ConditionKind            outputKind )
{
   if( outputKind == ConditionKind::RAW ) m_performsConditionIO = true;
   return registerOutputImpl<T>( outputID, outputKind );
}

#else

template< typename T >
const ConditionWriteHandle<T> &
ConditionAlgBase::registerOutput( const framework::ConditionID & outputID )
{
   return registerOutputImpl<T>( outputID, ConditionKind::DERIVED );
}

#endif


template< typename T >
const ConditionWriteHandle<T> &
ConditionAlgBase::registerOutputImpl( const framework::ConditionID & outputID,
                                      const ConditionKind            outputKind )
{
   // Get a write handle to our output and put it in m_outputHandles
   m_outputHandles.emplace_back(
      m_conditionService.transientStore().registerOutput<T>(
         reinterpret_cast<framework::ConditionUserID>( this ),
         outputID,
         outputKind
      )
   );

   // Provide a reference to the input to the client
   return m_outputHandles.back().get<T>();
}
