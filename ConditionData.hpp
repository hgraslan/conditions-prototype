#ifndef CONDITION_DATA_HPP
#define CONDITION_DATA_HPP

#include "framework/timing.hpp"


// Time-dependent condition data blob
template< typename T >
struct ConditionData
{
   T value;                      // Concrete data blob
   framework::TimeInterval iov;  // Range of timestamps for which this blob is valid
};

#endif
