#ifndef CONDITION_HANDLE_HPP
#define CONDITION_HANDLE_HPP

#include "ConditionData.hpp"
#include "ConditionSlot.hpp"
#include "exceptions.hpp"

#include "detail/ConditionHandleImpl.hpp"
#include "detail/SharedConditionData.hpp"


// Forward declaration of other prototype components
class TransientConditionStorageSvc;


// Condition handles are a proxy object to condition data. All condition handles have some functionality in common.
//
class ConditionHandleBase
{
public:

   // They are movable, but not copyable: you should not share your access to a condition with another Gaudi component,
   // that component needs to go through the TransientConditionStorageSvc as well. This allows tracking dependencies.
   ConditionHandleBase( const ConditionHandleBase & ) = delete;
   ConditionHandleBase( ConditionHandleBase && ) = default;
   ConditionHandleBase & operator=( const ConditionHandleBase & ) = delete;
   ConditionHandleBase & operator=( ConditionHandleBase && ) = default;

   // We will sometimes type-erase ConditionHandles as ConditionHandleBase, which requires a virtual destructor
   virtual ~ConditionHandleBase() = default;

   // They can tell you whether the associated condition data has been inserted in a certain slot or not
   bool hasValue( const ConditionSlot & slot ) const;


protected:

   // They are spawned by the TransientConditionStorageSvc, using an implementation from the ConditionStore.
   friend class TransientConditionStorageSvc;
   ConditionHandleBase( detail::ConditionHandleImpl && impl );
   detail::ConditionHandleImpl m_impl;

};


// In addition to this, read handles let you read condition data in a thread-safe and type-safe way
//
template< typename T >
class ConditionReadHandle : public ConditionHandleBase
{
public:

   // Build from a ConditionHandleBase
   ConditionReadHandle( ConditionHandleBase && base );

   // Provide read-only access to the condition data
   const ConditionData<T> & get( const ConditionSlot & slot ) const;

};


// And write handles let a single writer set the value of a condition (but not modify it)
//
template< typename T >
class ConditionWriteHandle : public ConditionHandleBase
{
public:

   // Build from a ConditionHandleBase
   ConditionWriteHandle( ConditionHandleBase && base );

   // Move a condition data blob into transient storage
   void put( const ConditionSlot    &  slot,
                   ConditionData<T> && value ) const;

   // Copy a condition data blob into transient storage
   void put( const ConditionSlot    & slot,
             const ConditionData<T> & value ) const;

   // Attempting to change the value of a condition that was previously set is an error
   class ConditionAlreadySet : public ConditionPrototypeException { };


private:

   // The copy and move versions of "put" have large common subset, which is implemented here
   void putBlob( const ConditionSlot               &  slot,
                       detail::SharedConditionData && blob ) const;

};


// Inline implementations
#include "ConditionHandle.ipp"

#endif
