inline
ConditionHandleBase::ConditionHandleBase( detail::ConditionHandleImpl && impl )
   : m_impl{ std::move(impl) }
{ }


inline
bool
ConditionHandleBase::hasValue( const ConditionSlot & slot ) const
{
   return m_impl.blobExists( slot );
}


template< typename T >
ConditionReadHandle<T>::ConditionReadHandle( ConditionHandleBase && base )
   : ConditionHandleBase{ std::move(base) }
{ }


template< typename T >
const ConditionData<T> &
ConditionReadHandle<T>::get( const ConditionSlot & slot ) const
{
   // Access the type-erased condition data
   const auto & data = m_impl.getBlob( slot );
   
   // Unpack it and return a reference to the actual condition data
   return data.get<T>();
}


template< typename T >
ConditionWriteHandle<T>::ConditionWriteHandle( ConditionHandleBase && base )
   : ConditionHandleBase{ std::move(base) }
{ }


template< typename T >
void
ConditionWriteHandle<T>::put( const ConditionSlot    &  slot,
                                    ConditionData<T> && value ) const
{
   putBlob(
      slot,
      detail::SharedConditionData{ std::move(value) }
   );
}


template< typename T >
void
ConditionWriteHandle<T>::put( const ConditionSlot    & slot,
                              const ConditionData<T> & value ) const
{
   putBlob(
      slot,
      detail::SharedConditionData{ value }
   );
}


template< typename T >
void
ConditionWriteHandle<T>::putBlob( const ConditionSlot               &  slot,
                                        detail::SharedConditionData && blob ) const
{
   // Try to insert the condition into the storage
   bool result = m_impl.tryPutBlob(
      slot,
      std::move(blob)
   );

   // Throw an exception if the condition was already set
   if( !result ) throw ConditionAlreadySet{};
}
