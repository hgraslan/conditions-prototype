#include <algorithm>

#include "ConditionIOSvcBase.hpp"
#include "ConditionSvc.hpp"


ConditionIOSvcBase::ConditionIOSvcBase(       ConditionSvc              & conditionService,
                                        const framework::ConditionIDSet & expectedOutputs )
   : m_conditionService( conditionService )
{
   // Take note of our expected outputs, note that none of them is initially registered
   m_registeredOutputs.reserve( expectedOutputs.size() );
   for( const auto & expectedOutput : expectedOutputs ) {
      m_registeredOutputs.emplace( expectedOutput, false );
   }

   // Register ourselves to the ConditionService
   conditionService.registerConditionProducer( *this );
}


bool
ConditionIOSvcBase::conditionOutputsRegistered() const
{
   return std::all_of(
      m_registeredOutputs.cbegin(),
      m_registeredOutputs.cend(),
      []( const OutputRegistrationStatus::value_type & value ) -> bool {
         return value.second;
      }
   );
}
