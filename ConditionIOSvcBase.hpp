#ifndef CONDITION_IO_SVC_BASE_HPP
#define CONDITION_IO_SVC_BASE_HPP

#include <unordered_map>

#include "ConditionHandle.hpp"
#include "exceptions.hpp"

#include "framework/IConditionIOSvc.hpp"
#include "framework/identifiers.hpp"


// Forward declaration of other prototype components
class ConditionSvc;


// This class is intended as a base class to all condition IO services. It provides some general services which
// each condition IO service is expected to need during implementation.
class ConditionIOSvcBase : public framework::IConditionIOSvc
{
public:

   // If it turns out, at initialization or event processing time, that a ConditionIOSvc cannot
   // produce one of its expected condition outputs, the following exception will be thrown:
   class CannotProduceCondition : public ConditionPrototypeException { };


protected:

   // Condition IO services must be registered to the ConditionSvc at initialization time.
   // The client will provide them with a list of conditions that they expect them to produce.
   ConditionIOSvcBase(       ConditionSvc              & conditionService,
                       const framework::ConditionIDSet & expectedOutputs );

   // Write handles to the outputs may subsequently be registered by the service using the following method.
   // If one attempts to register an output which was not declared at initialization time, or to
   // register a single output twice, PrototypeBugDetected will be thrown.
   template< typename T >
   ConditionWriteHandle<T> registerOutput( const framework::ConditionID & outputID );
   
   // A daughter class of ConditionIOSvc can check that all expected condition outputs have been registered
   bool conditionOutputsRegistered() const;


private:

   // Keep track of the location of the condition service
   const ConditionSvc & m_conditionService;
   
   // Keep track of which outputs have been registered so far
   using OutputRegistrationStatus = std::unordered_map< framework::ConditionID, bool >;
   OutputRegistrationStatus m_registeredOutputs;

};


// Inline implementations
#include "ConditionIOSvcBase.ipp"

#endif
