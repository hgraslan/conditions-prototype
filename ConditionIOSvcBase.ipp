#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"


template< typename T >
ConditionWriteHandle<T>
ConditionIOSvcBase::registerOutput( const framework::ConditionID & productID )
{
   // Make sure that the proposed condition is an expected output
   const auto outputIt = m_registeredOutputs.find( productID );
   if( outputIt == m_registeredOutputs.cend() ) throw PrototypeBugDetected{};
   
   // Make sure that each output is only registered once
   bool & outputRegistered = outputIt->second;
   if( outputRegistered ) {
      throw PrototypeBugDetected{};
   } else {
      outputRegistered = true;
   }
   
   // Get a write handle to the proposed condition
   return m_conditionService.transientStore().registerOutput<T>(
      reinterpret_cast<framework::ConditionUserID>( this ),
      productID,
      ConditionKind::RAW
   );
}
