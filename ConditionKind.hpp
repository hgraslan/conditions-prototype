#ifndef CONDITION_KIND_HPP
#define CONDITION_KIND_HPP

// We want to distinguish raw conditions, which bring in new authoritative information, generally from remote
// sources (databases, files...), and derived conditions, which are computed locally from raw conditions.
//
// For example, a detector alignment matrix is a raw condition, and an aligned detector geometry computed by
// applying this alignment matrix to a reference geometry is a derived condition.
//
enum struct ConditionKind { RAW, DERIVED };

#endif
