#ifndef CONDITION_MULTI_TRANSFORMER_HPP
#define CONDITION_MULTI_TRANSFORMER_HPP

#include <array>
#include <tuple>

#include "cpp_next/utility.hpp"

#include "ConditionHandle.hpp"
#include "ConditionSlot.hpp"

#include "detail/ConditionTransformerBase.hpp"

#include "framework/identifiers.hpp"
#include "framework/IScheduler.hpp"


// Forward declaration of other prototype components
class ConditionSvc;


// ConditionMultiTransformer is an adaptation of Gaudi::Functional::MultiTransformer to condition derivation.
// It helps writing condition derivation algorithms by automating the tedious work of fetching inputs, unpacking
// their value from the ConditionData, computing the IoV of the output, and writing the output down.
//
// Since this class assumes that algorithms are only used to produce derived conditions, it is NOT
// available when IO inside of ConditionAlgs is enabled.
//
#ifndef ALLOW_IO_IN_ALGORITHMS


// Like Gaudi::Functional::MultiTransformer, ConditionMultiTransformer is templated by a function signature.
// Since the C++ template system is broken beyond repair, we need to implement this through specialization.
template< typename >
class ConditionMultiTransformer;


// ConditionMultiTransformer acts as a base class to a concrete user-defined functor, which defines an operator()
// with the function signature given as a template parameter.
template< typename... Results,
          typename... Args >
class ConditionMultiTransformer< std::tuple<Results...>(Args...) > :
   public detail::ConditionTransformerBase< std::tuple<Results...>, Args... >
{
public:

   // Inherit useful declarations from the base class
   using Base = detail::ConditionTransformerBase< std::tuple<Results...>, Args... >;
   using typename Base::ConditionIDRef;
   using typename Base::ArgsIDs;

   // To initialize a ConditionMultiTransformer, a user must provide it with the location of the ConditionSvc
   // and specify the condition identifiers of its results and arguments.
   using ResultsIDs = std::array< ConditionIDRef, sizeof...(Results) >;
   ConditionMultiTransformer( ConditionSvc          &  conditionService,
                              framework::IScheduler &  scheduler,
                              ResultsIDs            && resultsIDs,
                              ArgsIDs               && argsIDs );

   // ConditionMultiTransformer can be scheduled by Gaudi as a ConditionAlg
   void execute( const ConditionSlot & slot ) const final override;


protected:

   // The user needs to implement the condition derivation process as a functor with the following signature:
   using ResultsTuple = std::tuple<Results...>;
   // ResultsTuple operator()( const Args &... ) const = 0;


private:

   // Since we have multiple outputs, we need something similar to registerArgs for them
   using ResultsHandles = std::tuple< std::reference_wrapper< const ConditionWriteHandle<Results> >... >;
   template< std::size_t... ResultIndexes >
   ResultsHandles registerResults( ResultsIDs && resultsIDs,
                                   cpp_next::index_sequence<ResultIndexes...> );

   // We need a layer of indirection below execute() in order to generate the index sequence that will be used
   // to access the algorithm's condition inputs and outputs.
   template< std::size_t... ArgsIndexes,
             std::size_t... ResultsIndexes >
   void executeImpl( const ConditionSlot & slot,
                     cpp_next::index_sequence<ResultsIndexes...>,
                     cpp_next::index_sequence<ArgsIndexes...> ) const;

   // The output handles are stored here
   const ResultsHandles resultsHandles;

};


// Since ConditionMultiTransformer is intended for condition derivation, algorithms without inputs are not allowed.
template< typename... Results >
class ConditionMultiTransformer< std::tuple< Results... >() >;


// Inline implementations
#include "ConditionMultiTransformer.ipp"


#endif

#endif
