#include "detail/run_statement_pack.hpp"


template< typename... Results,
          typename... Args >
void
ConditionMultiTransformer< std::tuple<Results...>(Args...) >::execute( const ConditionSlot & slot ) const
{
   executeImpl( slot,
                cpp_next::index_sequence_for<Results...>{},
                cpp_next::index_sequence_for<Args...>{} );
}


template< typename... Results,
          typename... Args >
ConditionMultiTransformer< std::tuple<Results...>(Args...) >::ConditionMultiTransformer(
   ConditionSvc          &  conditionService,
   framework::IScheduler &  scheduler,
   ResultsIDs            && resultsIDs,
   ArgsIDs               && argsIDs
)
   : Base{ conditionService, scheduler, std::move(argsIDs) }
   , resultsHandles{ registerResults( std::move(resultsIDs),
                                      cpp_next::index_sequence_for<Results...>{} ) }
{ }


template< typename... Results,
          typename... Args >
template< std::size_t... ResultsIndexes >
typename ConditionMultiTransformer< std::tuple<Results...>(Args...) >::ResultsHandles
ConditionMultiTransformer< std::tuple<Results...>(Args...) >::registerResults(
   ResultsIDs && resultsIDs,
   cpp_next::index_sequence<ResultsIndexes...>
) {
   // Build the tuple of ConditionWriteHandle references corresponding to the output results
   return ResultsHandles{ std::cref( ConditionAlgBase::registerOutput<Results>( resultsIDs[ResultsIndexes] ) )... };
}


template< typename... Results,
          typename... Args >
template< std::size_t... ArgsIndexes,
          std::size_t... ResultsIndexes >
void
ConditionMultiTransformer< std::tuple<Results...>(Args...) >::executeImpl(
   const ConditionSlot & slot,
   cpp_next::index_sequence<ResultsIndexes...>,
   cpp_next::index_sequence<ArgsIndexes...>
) const {
   // Fetch the arguments from the associated handles, derive the tuple of results and its IoV
   auto outputCondition = Base::deriveOutput(
      std::get<ArgsIndexes>( Base::argsHandles ).get().get( slot )...
   );
   auto & outputDataTuple = outputCondition.value;
   const auto & outputIoV = outputCondition.iov;
   
   // Move each result at its place within transient storage
   DETAIL_RUN_STATEMENT_PACK(
      std::get<ResultsIndexes>( resultsHandles ).get().put(
         slot,
         { std::move( std::get<ResultsIndexes>( outputDataTuple ) ), outputIoV }
      )
   );
}
