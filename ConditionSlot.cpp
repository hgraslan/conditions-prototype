#include "ConditionSlot.hpp"

#include "detail/ConditionStore.hpp"


ConditionSlot::~ConditionSlot()
{
   if( m_isOwner ) {
      m_host.get().liberateSlot( *m_id );
   }
}
