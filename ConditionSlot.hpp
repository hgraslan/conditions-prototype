#ifndef CONDITION_SLOT_HPP
#define CONDITION_SLOT_HPP

#include <functional>

#include "cpp_next/future.hpp"
#include "cpp_next/optional.hpp"

#include "MissingConditions.hpp"

#include "detail/ConditionSlotID.hpp"


// Forward declaration of other prototype components
namespace detail {
   class ConditionStore;
}


// This class is an RAII wrapper around a condition storage slot. It takes care of liberating it after event processing.
class ConditionSlot
{
public:

   // === BASIC WRAPPER OPERATION ===

   // ConditionSlots wrap an implementation-defined identifier of condition storage
   const detail::ConditionSlotID & id() const;

   // Every time an event is allocated a condition slot, the ConditionStore creates such a wrapper
   ConditionSlot( detail::ConditionSlotID   id,
                  detail::ConditionStore  & host,
                  bool                      isOwner = true );

   // The wrapper is movable but not copyable: the intended use is to put in the EventContext and keep it there
   ConditionSlot( const ConditionSlot & ) = delete;
   ConditionSlot( ConditionSlot && other );
   ConditionSlot & operator=( const ConditionSlot & ) = delete;
   ConditionSlot & operator=( ConditionSlot && other );

   // On destruction, the wrapper will make sure that the condition slot is liberated
   ~ConditionSlot();


   // === ADVANCED FEATURES ===

   // During condition slot initialization, before the condition slot has reached its final location, one
   // may need non-owning references to it, for example to carry out asynchronous IO.
   //
   // In this case, it is not safe to use a const ConditionSlot &, because that reference may be invalidated
   // if the ConditionSlot wrapper is moved. The following mechanism should be used instead.
   //
   ConditionSlot ref() const;

   // One can also check if two events use the same condition storage slot
   bool operator==( const ConditionSlot & other ) const;
   bool operator!=( const ConditionSlot & other ) const;


private:

   cpp_next::optional< detail::ConditionSlotID > m_id;
   std::reference_wrapper< detail::ConditionStore > m_host;
   bool m_isOwner;
   
};


// Condition slots are built through an iterative process (allocation -> IO -> derivation),
// each iteration telling the next which work remains to be done.
struct ConditionSlotIteration
{
   ConditionSlot slot;
   MissingConditions missingConditions;
};

// Iterations are asynchronous, one can synchronize with them and stack extra work on top of them through futures.
using ConditionSlotFuture = cpp_next::future< ConditionSlotIteration >;


// Inline implementations
#include "ConditionSlot.ipp"

#endif
