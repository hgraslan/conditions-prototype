inline
const detail::ConditionSlotID &
ConditionSlot::id() const
{
   return *m_id;
}


inline
ConditionSlot::ConditionSlot( detail::ConditionSlotID   id,
                              detail::ConditionStore  & host,
                              bool                      isOwner )
   : m_id{ std::move(id) }
   , m_host{ host }
   , m_isOwner{ isOwner }
{ }


inline
ConditionSlot::ConditionSlot( ConditionSlot && other )
   : m_id{ std::move(other.m_id) }
   , m_host{ std::move(other.m_host) }
   , m_isOwner{ other.m_isOwner }
{
   other.m_id.reset();
   other.m_isOwner = false;
}


inline
ConditionSlot &
ConditionSlot::operator=( ConditionSlot && other )
{
   if( &other == this ) return *this;

   m_id = std::move(other.m_id);
   m_host = std::move(other.m_host);
   m_isOwner = other.m_isOwner;
   other.m_id.reset();
   other.m_isOwner = false;

   return *this;
}


inline
ConditionSlot
ConditionSlot::ref() const
{
   return ConditionSlot{ *m_id, m_host.get(), false };
}


inline
bool
ConditionSlot::operator==( const ConditionSlot & other ) const
{
   return ( m_id == other.m_id );
}


inline
bool
ConditionSlot::operator!=( const ConditionSlot & other ) const
{
   return !( *this == other );
}
