#include "cpp_next/future.hpp"

#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/executors.hpp"
#include "detail/single_use_bind.hpp"
#include "detail/when_all.hpp"


ConditionSvc::ConditionSvc( TransientConditionStorageSvc & transientStore )
   : m_transientStore( transientStore )
{ }


TransientConditionStorageSvc &
ConditionSvc::transientStore() const
{
   return m_transientStore;
}


void
ConditionSvc::registerConditionProducer( framework::IConditionIOSvc & ioService )
{
   // Check that the condition producer is not already registered
   bool already_registered = std::any_of(
      m_ioServices.cbegin(),
      m_ioServices.cend(),
      [&ioService]( const std::reference_wrapper<framework::IConditionIOSvc> & candidate ) -> bool {
         return ( &(candidate.get()) == &ioService );
      }
   );
   if( already_registered ) throw ConditionProducerAlreadyRegistered{};
   
   // Register new condition producer
   m_ioServices.emplace_back( ioService );
}


ConditionSlotFuture
ConditionSvc::setupConditions( const framework::TimePoint & eventTimestamp )
{
   // Schedule allocation of a condition slot, followed by condition IO.
   //
   // DEBUG: Since the C++ concurrency TS does not currently provide a standard way to schedule
   //        continuations to be executed inline, we must use nonstandard Boost extensions here.
   //
   return ConditionSlotFuture{
      m_transientStore.allocateSlot( eventTimestamp ).then(
         detail::inlineContinuationExecutor,
         [this, eventTimestamp]( ConditionSlotFuture && slotFuture ) -> ConditionSlotFuture
         {
            // Fetch the condition slot from the input future
            ConditionSlotIteration slotIteration = slotFuture.get();
            
            // If no IO needs to be carried out, propagate the condition slot to the output right away
            if( slotIteration.missingConditions.raw.empty() ) {
               return cpp_next::make_ready_future( std::move(slotIteration) );
            }
            
            // Set up storage for the condition IO futures
            std::vector< cpp_next::future<void> > ioFutures;
            ioFutures.reserve( m_ioServices.size() );
            
            // Start all condition IO
            for( const auto & service : m_ioServices ) {
               ioFutures.emplace_back( service.get().startConditionIO( eventTimestamp, slotIteration ) );
            }
            
            // Prepare to manipulate the union of these IO futures, which represents IO completion
            using IOFuturesUnion = decltype( detail::when_all( ioFutures.begin(), ioFutures.end() ) );

            // After the IO is complete, we will check if it went well, update the missing condition metadata,
            // and push the updated condition slot down the processing chain
            auto ioContinuationImpl =
               []( ConditionSlotIteration && slotIteration, IOFuturesUnion && unionFuture ) -> ConditionSlotIteration
               {
                  unionFuture.get();
                  slotIteration.missingConditions.raw.clear();
                  return std::move(slotIteration);
               };

            // That continuation hence needs to take ownership of the condition slot.
            // In C++14, we'll do this directly in the lambda by using move capture and a mutable operator().
            auto ioContinuation = detail::single_use_bind(
               std::move(ioContinuationImpl),
               std::move(slotIteration),
               std::placeholders::_1
            );
            
            // Schedule the previously continuation to be executed after the IO is over
            return detail::when_all( ioFutures.begin(), ioFutures.end() ).then(
               detail::inlineContinuationExecutor,
               std::move(ioContinuation)
            );
         }
      )
   };
}
