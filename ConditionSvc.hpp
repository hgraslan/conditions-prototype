#ifndef CONDITION_SVC_HPP
#define CONDITION_SVC_HPP

#include <functional>
#include <vector>

#include "ConditionSlot.hpp"
#include "exceptions.hpp"

#include "framework/IConditionIOSvc.hpp"
#include "framework/timing.hpp"


// Forward declaration of other prototype components
class TransientConditionStorageSvc;


// Centralized entry point for condition infrastructure coordination and interactions with the Gaudi Scheduler.
class ConditionSvc
{
public:

   // To construct the ConditionSvc, we need a place to store conditions in RAM
   ConditionSvc( TransientConditionStorageSvc & transientStore );

   // The condition service is neither movable nor copyable
   ConditionSvc( const ConditionSvc & ) = delete;
   ConditionSvc( ConditionSvc && ) = delete;
   ConditionSvc & operator=( const ConditionSvc & ) = delete;
   ConditionSvc & operator=( ConditionSvc && ) = delete;

   // Access condition storage
   TransientConditionStorageSvc & transientStore() const;

   // Register a condition IO service
   void registerConditionProducer( framework::IConditionIOSvc & ioService );
   class ConditionProducerAlreadyRegistered : public ConditionPrototypeException { };

   // Setup the conditions infrastructure (allocate a storage slot, perform IO...) for a new event.
   // Returns a future that will eventually provide this event's condition slot, with all raw conditions present.
   // The Gaudi scheduler will be in charge of running the right ConditionAlgs to compute the derived conditions.
   ConditionSlotFuture setupConditions( const framework::TimePoint & eventTimestamp );


private:

   TransientConditionStorageSvc & m_transientStore;
   std::vector< std::reference_wrapper<framework::IConditionIOSvc> > m_ioServices;

};

#endif
