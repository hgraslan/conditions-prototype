#ifndef CONDITION_TRANSFORMER_HPP
#define CONDITION_TRANSFORMER_HPP

#include "cpp_next/utility.hpp"

#include "ConditionHandle.hpp"
#include "ConditionSlot.hpp"

#include "detail/ConditionTransformerBase.hpp"

#include "framework/identifiers.hpp"


// Forward declaration of other prototype components
class ConditionSvc;


// ConditionTransformer is an adaptation of the Gaudi::Functional::Transformer concept to condition derivation.
// It helps writing condition derivation algorithms by automating the tedious work of fetching inputs, unpacking
// their value from the ConditionData, computing the IoV of the output, and writing the output down.
//
// Since this class assumes that algorithms are only used to produce derived conditions, it is NOT
// available when IO inside of ConditionAlgs is enabled.
//
#ifndef ALLOW_IO_IN_ALGORITHMS


// Like Gaudi::Functional::Transformer, ConditionTransformer is templated by a function signature.
// Since the C++ template system is broken beyond repair, we need to implement this through specialization.
template< typename >
class ConditionTransformer;


// ConditionTransformer acts as a base class to a concrete user-defined functor, which defines an operator()
// with the function signature given as a template parameter.
template< typename Result,
          typename... Args >
class ConditionTransformer< Result(Args...) > :
    public detail::ConditionTransformerBase< Result, Args... >
{
public:

   // Inherit useful declarations from the base class
   using Base = detail::ConditionTransformerBase< Result, Args... >;
   using typename Base::ArgsIDs;

   // To initialize a ConditionTransformer, a user must provide it with the location of the ConditionSvc
   // and specify the condition identifiers of its result and arguments.
   ConditionTransformer(       ConditionSvc           &  conditionService,
                               framework::IScheduler  &  scheduler,
                         const framework::ConditionID &  resultID,
                               ArgsIDs                && argsIDs );

   // ConditionTransformer can be scheduled by Gaudi as a ConditionAlg
   void execute( const ConditionSlot & slot ) const final override;


protected:

   // The user needs to implement the condition derivation process as a functor with the following signature:
   // virtual Result operator()( const Args & ... args ) const = 0;


private:

   // We need a layer of indirection below execute() in order to generate the index sequence that will be used
   // to access the algorithm's condition inputs.
   template< std::size_t... ArgIndexes >
   void executeImpl( const ConditionSlot & slot,
                     cpp_next::index_sequence<ArgIndexes...> ) const;

   // The input and output handles are stored here
   const ConditionWriteHandle<Result> & resultHandle;

};


// Since ConditionTransformer is intended for condition derivation, algorithms without inputs are not allowed.
template< typename Result >
class ConditionTransformer< Result() >;


// Inline implementations
#include "ConditionTransformer.ipp"


#endif

#endif
