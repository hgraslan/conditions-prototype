template< typename Result,
          typename... Args >
void
ConditionTransformer< Result(Args...) >::execute( const ConditionSlot & slot ) const
{
   executeImpl( slot,
                cpp_next::index_sequence_for<Args...>{} );
}


template< typename Result,
          typename... Args >
ConditionTransformer< Result(Args...) >::ConditionTransformer(       ConditionSvc           &  conditionService,
                                                                     framework::IScheduler  &  scheduler,
                                                               const framework::ConditionID &  resultID,
                                                                     ArgsIDs                && argsIDs )
   : Base{ conditionService, scheduler, std::move(argsIDs) }
   , resultHandle( ConditionAlgBase::registerOutput<Result>( resultID ) )
{ }


template< typename Result,
          typename... Args >
template< std::size_t... ArgsIndexes >
void
ConditionTransformer< Result(Args...) >::executeImpl( const ConditionSlot & slot,
                                                      cpp_next::index_sequence<ArgsIndexes...> ) const
{
   // Fetch the arguments from the associated handles, derive the output, and put it in the result handle
   resultHandle.put(
      slot,
      Base::deriveOutput(
         std::get<ArgsIndexes>( Base::argsHandles ).get().get( slot )...
      )
   );
}
