#ifndef MISSING_CONDITIONS_HPP
#define MISSING_CONDITIONS_HPP

#include "framework/identifiers.hpp"


// During condition slot setup, we may have some missing conditions. The following struct will be used to track this metadata.
struct MissingConditions
{
   framework::ConditionIDSet raw;
   framework::ConditionIDSet derived;
   static const MissingConditions NO_MISSING_CONDITION;
};

#endif
