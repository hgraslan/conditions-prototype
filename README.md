# Condition handling prototype

## About this project

This is a prototype to the condition handling infrastructure that is being
proposed for inclusion in Gaudi. It was built around the following requirements:

* Safe to use in the presence of multiple event processing threads (Necessary in
  Gaudi Hive)
* Optional ability to handle multiple conditions concurrently (ATLAS may or may
  not need it, LHCb don't need it, and it has a complexity cost)
* Deadlock-safe bound on condition concurrency (To keep RAM usage in check and
  fulfill prior requirement)
* Abstraction of the underlying transient condition storage backend (Many
  options exist, and the landscape is changing rapidly, abstracting it allows
  algorithms and interfaces to persistent storage not to care, and even to be
  made experiment-independent in some cases)
* Easy to use correctly and hard to use incorrectly, for everyone (framework,
  event processing algs, condition derivation code, condition IO...)
* Keep implementation simple and efficient (eases maintenance)
* Achieve reasonable compatibility with the ATLAS condition infrastructure
  (avoids proliferation of incompatible interfaces, eases possible future
  migration of ATLAS)
* Make it possible to carry out condition IO in services, not algorithms (leads
  to much more efficient CPU and IO usage patterns, avoids deadlocks)


## Technical details

This prototype is entirely written in C++11, but uses the Boost library in order
to leverage some important evolutions of the C++ standard library that are
coming up in C++17 and beyond.

The build system is based on a very simple Makefile. This solution is admittedly
not terribly efficient nor portable, and will be replaced with CMake during
final Gaudi integration, but any Linux-based system with the following
dependencies installed should be able to run it:

* GNU Make >= 3.81
* g++ >= 4.8.4
* boost >= 1.60

To build the prototype, simply go to the root of the source tree and do

    make -j<N>

where `<N>` should be set to the number of CPU cores of your build machine.