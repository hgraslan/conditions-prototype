#include <algorithm>

#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"


TransientConditionStorageSvc::TransientConditionStorageSvc( const std::size_t    capacity )
   : m_storage( std::max(capacity, 1ul) )
{ }


std::size_t
TransientConditionStorageSvc::max_capacity()
{
   return UNBOUNDED_STORAGE;
}


std::size_t
TransientConditionStorageSvc::availableStorage()
{
   return m_storage.availableSlots();
}


ConditionSlotFuture
TransientConditionStorageSvc::allocateSlot( const framework::TimePoint & eventTimestamp )
{
   return m_storage.allocateSlot( eventTimestamp );
}


framework::ConditionDataflow::Metadata
TransientConditionStorageSvc::getConditionDataflow() const
{
   return m_dataflow.report();
}
