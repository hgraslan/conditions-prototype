#ifndef TRANSIENT_CONDITION_STORAGE_SVC_HPP
#define TRANSIENT_CONDITION_STORAGE_SVC_HPP

#include <limits>

#include "ConditionHandle.hpp"
#include "ConditionKind.hpp"
#include "ConditionSlot.hpp"
#include "exceptions.hpp"

#include "detail/ConditionStore.hpp"

#include "framework/ConditionDataflow.hpp"
#include "framework/identifiers.hpp"
#include "framework/timing.hpp"


// This class showcases a possible interface to concrete transient condition storage mechanisms:
//    - Mutable DetectorStore
//    - ATLAS ConditionStore
//    - DDCond (inside of DD4Hep)
//    - ...you name it!
//
class TransientConditionStorageSvc
{
public:

   // === INITIALIZATION & STORAGE BOUNDS MANAGEMENT ===
   
   // This dummy capacity value is used to leave storage capacity up to the implementation
   static const size_t UNBOUNDED_CAPACITY = 0;
   
   // This dummy storage size value is used to represent conceptually infinite (RAM-limited) storage size
   static const size_t UNBOUNDED_STORAGE = std::numeric_limits<size_t>::max();
   
   // A user should be able to bound condition storage to at most N complete sets of conditions ("slots").
   // This quantity is called the storage capacity. Specifying a capacity of 0 (UNBOUNDED_CAPACITY) means
   // that condition storage limits are left up to the implementation, and may be unbounded.
   //
   // Some implementations may only support unbounded or non-concurrent (capacity=1) storage.
   //
   TransientConditionStorageSvc( const size_t capacity );
   class UnsupportedStorageCapacity : public ConditionPrototypeException { };
   
   // This method indicates the maximum storage capacity supported by the active implementation.
   // If no conceptual limit exists, UNBOUNDED_STORAGE will be returned.
   static size_t max_capacity();
   
   // This method indicates how many condition storage slots are currently available.
   // If no conceptual limit exists, UNBOUNDED_STORAGE will be returned.
   size_t availableStorage();
   
   
   // === LIFE CYCLE ===
   
   // The transient condition storage service is neither movable nor copyable
   TransientConditionStorageSvc( const TransientConditionStorageSvc & ) = delete;
   TransientConditionStorageSvc( TransientConditionStorageSvc && ) = delete;
   TransientConditionStorageSvc & operator=( const TransientConditionStorageSvc & ) = delete;
   TransientConditionStorageSvc & operator=( TransientConditionStorageSvc && ) = delete;
   
   
   // === CONDITION DATA ACCESS (not thread-safe, initialization time only) ===
      
   // By registering an input, a Gaudi component notifies the condition management infrastructure that
   // its execution should be scheduled after the corresponding condition is produced. In exchange,
   // it gets access to the condition through the read handle mechanism
   template< typename T >
   ConditionReadHandle<T> registerInput( const framework::ConditionUserID & client,
                                         const framework::ConditionID     & targetID );

   // By registering an output, a Gaudi component identifies itself as the producer of a condition.
   // In exchange, it gets access to the condition through the write handle mechanism
   template< typename T >
   ConditionWriteHandle<T> registerOutput( const framework::ConditionUserID & client,
                                           const framework::ConditionID     & targetID,
                                           const ConditionKind                targetKind );
   
   
   // === STORAGE SLOT MANAGEMENT (thread-safe at event processing time) ===
   
   // Allocate a condition storage slot for an incoming event, reusing existing condition storage slot
   // and condition data. Slot allocation can be delayed if all condition slots are used up.
   // Returns a future that will be set when the storage slot is ready, and tell which conditions are missing.
   ConditionSlotFuture allocateSlot( const framework::TimePoint & eventTimestamp );
   
   
   // === DATAFLOW INTROSPECTION (thread-safe, after initialization) ===
   
   // Metadata about the condition dataflow (who writes what, who reads what) is available for the Scheduler
   framework::ConditionDataflow::Metadata getConditionDataflow() const;


private:

   // Internally, this class jointly manages condition storage and usage metadata
   detail::ConditionStore m_storage;
   framework::ConditionDataflow m_dataflow;

};


// Inline implementations
#include "TransientConditionStorageSvc.ipp"

#endif
