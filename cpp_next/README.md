This directory is a collection of features that are expected to be included in future versions of the C++ standard, but are either not there yet, or too fresh to be available in a typical libc++ implementation.

Isolating these features ensures that the future transition to standard C++ versions will be more painless.
