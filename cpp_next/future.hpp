#ifndef CPP_NEXT_FUTURE_HPP
#define CPP_NEXT_FUTURE_HPP

#define BOOST_THREAD_PROVIDES_FUTURE 1
#define BOOST_THREAD_PROVIDES_EXECUTORS 1
#define BOOST_THREAD_VERSION 4

#include <boost/thread/future.hpp>

#define REQUIRED_BOOST_VERSION 1.60
#define BOOST_VERSION_ERROR "Please use Boost >= "#REQUIRED_BOOST_VERSION"."

#ifndef BOOST_THREAD_PROVIDES_FUTURE_CONTINUATION
#  error "Your version of Boost does not provide future continuations. "#BOOST_VERSION_ERROR
#endif

#ifndef BOOST_THREAD_PROVIDES_FUTURE_UNWRAP
#  error "Your version of Boost does not provide future unwrapping. "#BOOST_VERSION_ERROR
#endif


namespace cpp_next
{

using namespace ::boost;

}

#endif
