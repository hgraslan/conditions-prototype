#ifndef CPP_NEXT_UTILITY_HPP
#define CPP_NEXT_UTILITY_HPP

#include <cstddef>


namespace cpp_next
{

// integer_sequence class template from C++14
template< class T, T... Ints >
struct integer_sequence
{
   using type = integer_sequence;
   using value_type = T;
   static constexpr std::size_t size() noexcept { return sizeof...(Ints); }
};


// index_sequence helper template from C++14
template< std::size_t... Ints >
using index_sequence = integer_sequence< std::size_t, Ints... >;


// Forward declaration of the make_index_sequence implementation
namespace make_index_sequence_impl { template< std::size_t > struct GeneratedSequence; }


// make_index_sequence helper template from C++14
template< std::size_t N >
using make_index_sequence = typename make_index_sequence_impl::GeneratedSequence<N>::type;


// index_sequence_for helper template from C++14
template< class... T >
using index_sequence_for = make_index_sequence< sizeof...(T) >;

}


// Inline implementations
#include "cpp_next/utility.ipp"

#endif
