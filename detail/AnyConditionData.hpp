#ifndef ANY_CONDITION_DATA_HPP
#define ANY_CONDITION_DATA_HPP

#include "cpp_next/any.hpp"

#include "ConditionData.hpp"

#include "framework/timing.hpp"


namespace detail
{

// Type-erased holder of ConditionData, for use in condition containers
class AnyConditionData
{
public:

   // Encapsulate a concrete condition data blob into an AnyConditionData holder
   template< typename T >
   AnyConditionData( ConditionData<T> && data );

   // Make an encapsulated copy of a condition data blob
   template< typename T >
   AnyConditionData( const ConditionData<T> & data );
   
   // Get read-only access to the underlying condition data. An unfortunate side-effect
   // of type erasure is that you need to specify which type of condition you expect.
   // If you specify the wrong type, InconsistentConditionType will be thrown at runtime.
   template< typename T >
   const ConditionData<T> & get() const;
   
   // Access the interval of validity of the type-erased condition
   const framework::TimeInterval & iov() const;

   
private:

   const cpp_next::any m_holder;
   const framework::TimeInterval & m_iov;

};

}


// Inline implementations
#include "detail/AnyConditionData.ipp"

#endif
