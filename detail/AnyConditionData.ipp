#include "exceptions.hpp"


namespace detail
{

template< typename T >
AnyConditionData::AnyConditionData( ConditionData<T> && data )
   : m_holder{ std::move(data) }
   , m_iov( cpp_next::any_cast<ConditionData<T>>( &m_holder )->iov )
{ }


template< typename T >
AnyConditionData::AnyConditionData( const ConditionData<T> & data )
   : m_holder{ data }
   , m_iov( cpp_next::any_cast<ConditionData<T>>( &m_holder )->iov )
{ }


template< typename T >
const ConditionData<T> &
AnyConditionData::get() const
{
   // Check that the right version of get() was called
   if( m_holder.type() != typeid(ConditionData<T>) ) throw InconsistentConditionType{};
   
   // Acquire a const pointer to the internal condition data
   const ConditionData<T> * result_ptr = cpp_next::any_cast<ConditionData<T>>( &m_holder );
   
   // Return the result as a const reference
   return *result_ptr;
}


inline
const framework::TimeInterval &
AnyConditionData::iov() const
{
   return m_iov;
}

}
