namespace detail
{

template< typename T >
AnyConditionReadHandle::AnyConditionReadHandle( ConditionReadHandle<T> && handle )
   : m_impl{ std::move(handle) }
{ }


template< typename T >
const ConditionReadHandle<T> &
AnyConditionReadHandle::get() const
{
   return m_impl.get< ConditionReadHandle<T> >();
}


inline
bool
AnyConditionReadHandle::hasValue( const ConditionSlot & slot ) const
{
   return m_impl.hasValue( slot );
}


template< typename T >
AnyConditionWriteHandle::AnyConditionWriteHandle( ConditionWriteHandle<T> && handle )
   : m_impl{ std::move(handle) }
{ }


template< typename T >
const ConditionWriteHandle<T> &
AnyConditionWriteHandle::get() const
{
   return m_impl.get< ConditionWriteHandle<T> >();
}


inline
bool
AnyConditionWriteHandle::hasValue( const ConditionSlot & slot ) const
{
   return m_impl.hasValue( slot );
}

}
