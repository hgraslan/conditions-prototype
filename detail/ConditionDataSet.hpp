#ifndef CONDITION_DATA_SET_HPP
#define CONDITION_DATA_SET_HPP

#include <vector>

#include "ConditionData.hpp"
#include "exceptions.hpp"

#include "framework/timing.hpp"


namespace detail
{

// Set of condition data blobs with non-null and non-overlapping IoV, optimized for fast lookup.
// This class is meant as a way to provide test input to the prototype.
template< typename T >
class ConditionDataSet
{
public:

   // Keep track of the kind of condition data that we are manipulating
   using ConditionDataType = ConditionData<T>;

   // Initialize from a user-provided dataset, to be checked for IoV nullness and overlap.
   ConditionDataSet( const std::vector< ConditionDataType > & init );
   class NullIoVDetected : public ConditionPrototypeException { };
   class OverlappingIoVDetected : public ConditionPrototypeException { };

   // Look for the condition data associated with a certain timestamp.
   // In our test input scenario, the condition data should always be there.
   const ConditionDataType & find( const framework::TimePoint & eventTimestamp ) const;
   class MissingConditionData : public ConditionPrototypeException { };


private:

   // Because this class is only intended as a test input, we can afford the memory overhead of
   // sorting the condition data in place. A more clever implementation could leave the concrete
   // condition data unsorted and solely sort an IoV-based index pointing to this data.
   std::vector< ConditionDataType > m_sorted_data;

};

}


// Inline implementations
#include "detail/ConditionDataSet.ipp"

#endif
