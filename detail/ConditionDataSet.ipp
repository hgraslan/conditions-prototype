#include <algorithm>


namespace detail
{

template< typename T >
ConditionDataSet<T>::ConditionDataSet( const std::vector< ConditionData<T> > & init )
   : m_sorted_data{ init }
{
   // Sort the input dataset according to the start of its interval of validity
   std::sort(
      m_sorted_data.begin(),
      m_sorted_data.end(),
      []( const ConditionDataType & a, const ConditionDataType & b ) -> bool {
         return a.iov.start() < b.iov.start();
      }
   );
   
   // Detect null IoVs in the input dataset
   const auto nullIovIterator = std::find_if(
      m_sorted_data.cbegin(),
      m_sorted_data.cend(),
      []( const ConditionDataType & c ) -> bool {
         return c.iov.isNull();
      }
   );
   if( nullIovIterator != m_sorted_data.cend() ) throw NullIoVDetected{};
   
   // Detect overlapping IoVs in the input dataset
   const auto iovOverlapIterator = std::adjacent_find(
      m_sorted_data.cbegin(),
      m_sorted_data.cend(),
      []( const ConditionDataType & a, const ConditionDataType & b ) -> bool {
         return b.iov.start() < a.iov.end();
      }
   );
   if( iovOverlapIterator != m_sorted_data.cend() ) throw OverlappingIoVDetected{};
}


template< typename T >
const ConditionData<T> & ConditionDataSet<T>::find( const framework::TimePoint & eventTimestamp ) const
{
   // Look for the first piece of condition data whose IoV end lies beyond the event timestamp
   const auto matchingIterator = std::lower_bound(
      m_sorted_data.cbegin(),
      m_sorted_data.cend(),
      eventTimestamp,
      []( const ConditionDataType & data, const framework::TimePoint & eventTimestamp ) -> bool {
         return data.iov.end() < eventTimestamp;
      }
   );
   
   // Make sure that such condition data exists
   if( matchingIterator == m_sorted_data.cend() ) throw MissingConditionData{};
   const auto & conditionData = *matchingIterator;
   
   // Because the condition data is sorted, if this piece of condition data is not suitable, no other will be
   if( !conditionData.iov.contains( eventTimestamp ) ) throw MissingConditionData{};
   return conditionData;
}

}
