#include <algorithm>

#include "ConditionHandleImpl.hpp"
#include "ConditionStore.hpp"
#include "exceptions.hpp"

#include "detail/ConditionSlotPromises.hpp"


namespace detail
{

bool
ConditionHandleImpl::blobExists( const ConditionSlot & slot ) const
{
   return targetData(slot).exists();
}


const SharedConditionData &
ConditionHandleImpl::getBlob( const ConditionSlot & slot ) const
{
   return targetData(slot);
}


bool
ConditionHandleImpl::tryPutBlob( const ConditionSlot       &  slot,
                                       SharedConditionData && blob ) const
{
   // Select the appropriate condition blob for the active slot
   auto & currentBlob = targetData(slot);
   
   // Abort if the condition has already been inserted
   if( currentBlob.exists() ) return false;
   
   // Otherwise, insert the condition into the condition store
   ReadySlotPromiseList readyPromises;
   {
      // This operation needs to be synchronized with the ConditionStore's internal bookkeeping,
      // so acquire the store's internal lock before proceeding
      ConditionStore & store = targetStore();
      auto lock = store.acquireLock();
      
      // Write the condition blob down (synchronizing with the reads in ConditionStore::setupSlot())
      currentBlob = std::move(blob);
      
      // Notify the ConditionStore that a condition has been inserted
      readyPromises = store.notifyConditionInsertion( slot.id(),
                                                      targetKind(),
                                                      currentBlob.iov() );
   }
   
   // Process all the slot allocation promises that were fulfilled thanks to this insertion
   for( auto & promise : readyPromises ) { 
      promise.process();
   }
   
   return true;
}


ConditionHandleImpl::ConditionHandleImpl( ConditionStore     & store,
                                          VersionedCondition & condition )
   : m_store( store )
   , m_condition( condition )
{ }


SharedConditionData &
ConditionHandleImpl::targetData( const ConditionSlot & slot ) const
{
   return m_condition.get().data[ slot.id() ];
}


ConditionKind
ConditionHandleImpl::targetKind() const
{
   return *m_condition.get().kind;
}


ConditionStore &
ConditionHandleImpl::targetStore() const
{
   return m_store.get();
}

}
