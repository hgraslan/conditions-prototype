#ifndef CONDITION_HANDLE_IMPLEMENTATION_HPP
#define CONDITION_HANDLE_IMPLEMENTATION_HPP

#include <functional>

#include "ConditionKind.hpp"
#include "ConditionSlot.hpp"

#include "detail/ConditionStoreInternals.hpp"
#include "detail/SharedConditionData.hpp"


namespace detail
{

// Forward declaration of other prototype components
class ConditionStore;


// ConditionHandles are spawned by the ConditionStore as a proxy to some
// time-dependent condition data. All handles share some common low-level
// functionality, which is grouped together in this class.
//
class ConditionHandleImpl
{
public:

   // Condition handle implementations are movable, but not copyable. Movability allows a condition client
   // to delegate handle creation to another Gaudi component, but the only use case for copyability would be
   // to sneak condition dependencies out of the Scheduler's sight, which cannot be allowed.
   ConditionHandleImpl( const ConditionHandleImpl & ) = delete;
   ConditionHandleImpl( ConditionHandleImpl && ) = default;
   ConditionHandleImpl & operator=( const ConditionHandleImpl & ) = delete;
   ConditionHandleImpl & operator=( ConditionHandleImpl && ) = default;

   // Tell whether the underlying condition has been set for the current event
   bool blobExists( const ConditionSlot & slot ) const;
   
   // Access the condition data blob for the current event
   const SharedConditionData & getBlob( const ConditionSlot & slot ) const;
   
   // Try to insert a condition data blob for the current event,
   // return false if the condition has already been set.
   bool tryPutBlob( const ConditionSlot       &  slot,
                          SharedConditionData && blob ) const;
   

private:

   // Only the ConditionStore is allowed to spawn ConditionHandles
   friend class ConditionStore;
   ConditionHandleImpl( ConditionStore     & store,
                        VersionedCondition & condition );
   
   // Access the condition data for one slot
   SharedConditionData & targetData( const ConditionSlot & slot ) const;
   
   // Access the condition kind
   ConditionKind targetKind() const;
   
   // Access the underlying ConditionStore
   ConditionStore & targetStore() const;

   // A ConditionHandle is an interface to a ConditionStore
   std::reference_wrapper< ConditionStore > m_store;

   // A ConditionHandle points to versioned condition data
   std::reference_wrapper< VersionedCondition > m_condition;

};

}

#endif
