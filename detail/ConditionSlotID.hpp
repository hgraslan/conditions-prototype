#ifndef CONDITION_SLOT_ID_HPP
#define CONDITION_SLOT_ID_HPP

#include <cstddef>


namespace detail
{

// Condition slots are internally identified using a integer index
using ConditionSlotID = std::size_t;

}

#endif
