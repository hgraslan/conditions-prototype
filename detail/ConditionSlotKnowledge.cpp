#include <algorithm>

#include "exceptions.hpp"

#include "detail/ConditionSlotKnowledge.hpp"


namespace detail
{

ConditionSlotKnowledge::ConditionSlotKnowledge( const size_t capacity )
   : m_capacity{ capacity }                                         // Remember how many slots we have around
   , m_slotUsage( capacity, 0 )                                     // All slots are free for use
   , m_slotIoV( capacity, framework::TimeInterval::NULL_INTERVAL )  // All slots must be filled with data before use
   , m_missingConditions( capacity, 0 )                             // No slot is being constructed
{ }


VersionedCondition &
ConditionSlotKnowledge::registerCondition( const framework::ConditionID & id,
                                           const OptionalConditionKind  & kind )
{
   // Make sure that this condition has associated storage
   auto emplaceResult = m_data.emplace( id, VersionedCondition{ kind, m_capacity } );
   auto & versionedCondition = emplaceResult.first->second;
   
   // Lazily set the condition kind as soon as it becomes available (when the writer is registered)
   if( kind ) versionedCondition.kind = kind;
   
   // Give the caller access to the versioned condition data. The reason we can share a reference here
   // is that unordered_map insertion does not invalidate previously acquired references to data.
   return versionedCondition;
}


bool
ConditionSlotKnowledge::reserveSuitableSlot( const framework::TimePoint & eventTimestamp,
                                                   ConditionSlotID      & slotID )
{
   return reserveSlotIf(
      [this, &eventTimestamp]( ConditionSlotID id ) -> bool {
         return m_slotIoV[id].contains( eventTimestamp );
      },
      slotID
   );
}


bool
ConditionSlotKnowledge::reserveUnusedSlot( ConditionSlotID & slotID )
{
   return reserveSlotIf(
      [this]( ConditionSlotID id ) -> bool {
         return ( m_slotUsage[id] == 0 );
      },
      slotID
   );
}


void
ConditionSlotKnowledge::reserveSlot( const ConditionSlotID slotID )
{
   ++m_slotUsage[slotID];
}


bool
ConditionSlotKnowledge::releaseSlot( const ConditionSlotID slotID )
{
   return ( --m_slotUsage[slotID] == 0 );
}


MissingConditions
ConditionSlotKnowledge::setupSlot( const ConditionSlotID        slotID,
                                   const framework::TimePoint & eventTimestamp )
{
   // Initialize missing condition tracking
   MissingConditions missingConditions;
   auto rawConditionInserter = std::inserter( missingConditions.raw, missingConditions.raw.end() );
   auto derivedConditionInserter = std::inserter( missingConditions.derived, missingConditions.derived.end() );
   
   // Initialize raw condition tracking
   m_incomingSlotIoV = framework::TimeInterval::INFINITE_INTERVAL;
   m_slotIoV[slotID] = framework::TimeInterval::NULL_INTERVAL;

   // For each condition that we manage...
   for( auto & mapItem : m_data )
   {
      // ...access the versioned dataset
      const auto & conditionID = mapItem.first;
      auto & versionedCondition = mapItem.second;
      
      // Look for a condition version that is suitable for the current event
      const auto & suitableVersionIterator = std::find_if(
         versionedCondition.data.cbegin(),
         versionedCondition.data.cend(),
         [&eventTimestamp]( const SharedConditionData & candidate ) -> bool {
            return candidate.validFor( eventTimestamp );
         }
      );
      
      // Set up the new condition slot according to our findings
      if( suitableVersionIterator != versionedCondition.data.end() )
      {
         const auto & suitableConditionData = *suitableVersionIterator;
         m_incomingSlotIoV.intersectWith( suitableConditionData.iov() );
         versionedCondition.data[slotID] = suitableConditionData;
      }
      else
      {
         switch( *versionedCondition.kind ) {
            case ConditionKind::RAW:
               rawConditionInserter = conditionID ;
               break;
            case ConditionKind::DERIVED:
               derivedConditionInserter = conditionID;
               break;
            default:
               throw PrototypeBugDetected{};
         }
         versionedCondition.data[slotID].clear();
      }
   }
   
   // Update our internal metadata to match the set of missing conditions that we collected
   m_missingRawConditions = missingConditions.raw.size();
   m_missingConditions[slotID] = m_missingRawConditions + missingConditions.derived.size();
   
   // There should be missing raw conditions here. Otherwise, we would not
   // need to create a new slot, but could be reusing an existing one instead.
   if( m_missingRawConditions == 0 ) throw PrototypeBugDetected{};

   // Propagate the set of missing conditions to the caller
   return missingConditions;
}


ConditionSlotKnowledge::SlotCompletionEvents
ConditionSlotKnowledge::notifyConditionInsertion( const ConditionSlotID           slotID,
                                                  const ConditionKind             kind,
                                                  const framework::TimeInterval & iov )
{
   SlotCompletionEvents outcome = { false, false };

   // Keep track of the fact that a new condition was inserted
   if( --m_missingConditions[slotID] == 0 ) {
      outcome.slotFullyConstructed = true;
   }

   // If this was a raw condition, we also need to update the IoV metadata accordingly
   if( kind == ConditionKind::RAW )
   {
      m_incomingSlotIoV.intersectWith( iov );
      if( --m_missingRawConditions == 0 ) {
         m_slotIoV[slotID] = m_incomingSlotIoV;
         outcome.slotValidityAvailable = true;
      }
   }
   
   return outcome;
}


size_t
ConditionSlotKnowledge::availableSlots() const
{
   return std::count_if(
      m_slotUsage.cbegin(),
      m_slotUsage.cend(),
      []( const int referenceCount ) -> bool {
         return ( referenceCount == 0 );
      }
   );
}


const framework::TimeInterval &
ConditionSlotKnowledge::slotIoV( const ConditionSlotID slotID ) const
{
   return m_slotIoV[slotID];
}


bool
ConditionSlotKnowledge::rawConditionsMissing() const
{
   return ( m_missingRawConditions != 0 );
}


bool
ConditionSlotKnowledge::slotFullyConstructed( const ConditionSlotID slotID ) const
{
   return ( m_missingConditions[slotID] == 0 );
}


bool
ConditionSlotKnowledge::reserveSlotIf( const SlotPredicate   & criterion,
                                             ConditionSlotID & slotID )
{
   // Try to reserve a slot that matches the provided criterion
   for( ConditionSlotID id = 0; id < m_capacity; ++id )
   {
      if( criterion(id) ) {
         reserveSlot( id );
         slotID = id;
         return true;
      }
   }
   
   // If control reaches this point, no suitable slot has been found
   return false;
}

}
