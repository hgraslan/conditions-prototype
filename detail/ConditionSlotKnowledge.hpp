#ifndef CONDITION_SLOT_KNOWLEDGE_HPP
#define CONDITION_SLOT_KNOWLEDGE_HPP

#include <functional>
#include <unordered_map>

#include "ConditionKind.hpp"
#include "MissingConditions.hpp"

#include "detail/ConditionStoreInternals.hpp"
#include "detail/ConditionSlotID.hpp"
#include "detail/OptionalConditionKind.hpp"

#include "framework/identifiers.hpp"
#include "framework/timing.hpp"


namespace detail
{

// This class is a subsystem of the ConditionStore which tracks the knowledge that we have
// about the condition slots in flight.
class ConditionSlotKnowledge
{
public:

   // === SCHEMA INITIALIZATION ===

   // Setup condition knowledge storage for "capacity" condition slots
   ConditionSlotKnowledge( const size_t capacity );
   
   // Register a (possibly new) condition, get fast access to the associated condition data storage
   VersionedCondition & registerCondition( const framework::ConditionID & id,
                                           const OptionalConditionKind  & kind );


   // === SLOT ALLOCATION AND LIBERATION ===
   
   // Attempt to reserve a condition slot whose IoV includes a certain timestamp.
   // Returns true and stores slot id in "slotID" if successful, returns false otherwise.
   bool reserveSuitableSlot( const framework::TimePoint & eventTimestamp,
                                   ConditionSlotID      & slotID );

   // Attempt to reserve a condition slot that is not currently being used, works like reserveSuitableSlot
   bool reserveUnusedSlot( ConditionSlotID & slotID );
   
   // Unconditionally reserve a specific condition slot
   void reserveSlot( const ConditionSlotID slotID );
   
   // Release a slot, tell whether it is ready for reuse or still has other users
   bool releaseSlot( const ConditionSlotID slotID );
   
   
   // === SLOT CONSTRUCTION ===
   
   // Setup a condition slot for use by a certain event, reusing as much condition data as possible.
   // Tell which condition data is missing from the newly created slot and must be regenerated.
   MissingConditions setupSlot( const ConditionSlotID        slotID,
                                const framework::TimePoint & eventTimestamp );

   // Writing in a new condition can result in a slot's IoV being determined and/or in an incomplete slot
   // being completed. These events will be reported through the following data structure.
   struct SlotCompletionEvents
   {
      bool slotValidityAvailable;
      bool slotFullyConstructed;
   };

   // Update condition slot metadata after a new condition was written in by a ConditionWriteHandle,
   // and report any of the aforementioned events
   SlotCompletionEvents notifyConditionInsertion( const ConditionSlotID           slotID,
                                                  const ConditionKind             kind,
                                                  const framework::TimeInterval & iov );


   // === SLOT QUERIES ===

   // Tell how many condition slots are currently available for use
   size_t availableSlots() const;
   
   // Access the IoV of a given slot
   const framework::TimeInterval & slotIoV( const ConditionSlotID slotID ) const;
   
   // Tell whether some condition slot is missing raw conditions, and thus has an unknown IoV
   bool rawConditionsMissing() const;
   
   // Tell whether a condition slot is fully constructed, or still missing some conditions
   bool slotFullyConstructed( const ConditionSlotID slotID ) const;
   

private:

   // === INTERNAL METHODS ===
   
   // Attempt to find and reserve a slot that fulfills some criterion, works like reserveSuitableSlot
   using SlotPredicate = std::function< bool(ConditionSlotID) >;
   bool reserveSlotIf( const SlotPredicate   & criterion,
                             ConditionSlotID & slotID );
   
   
   // === CONDITION DATA AND METADATA ===

   // Amount of conditions managed by the storage implementation
   const size_t m_capacity;

   // Versioned condition data
   using ConditionMap = std::unordered_map< framework::ConditionID, VersionedCondition >;
   ConditionMap m_data;
   
   // Condition slot usage tracking (for garbage collection)
   SlotData< int > m_slotUsage;
   
   // Interval of validity of each slot (= intersection of all raw condition IoVs)
   SlotData< framework::TimeInterval > m_slotIoV;

   // Missing raw conditions and associated work-in-progress IoV computations
   int m_missingRawConditions = 0;
   framework::TimeInterval m_incomingSlotIoV = framework::TimeInterval::NULL_INTERVAL;
   
   // Missing conditions in general, used to track incomplete slots
   SlotData< int > m_missingConditions;

};

}

#endif
