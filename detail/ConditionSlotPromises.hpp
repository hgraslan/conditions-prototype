#ifndef CONDITION_SLOT_PROMISES_HPP
#define CONDITION_SLOT_PROMISES_HPP

#include <vector>

#include "cpp_next/future.hpp"

#include "ConditionSlot.hpp"


namespace detail
{

// To asynchronously signal condition slot futures, we needs promises
using ConditionSlotPromise = cpp_next::promise< ConditionSlotIteration >;

// As the continuations to these futures might call ConditionStore methods, we need to make sure that
// the ConditionStore is in a consistent state before signalling these promises.
// The following helper class will be used to transmit future signaling homework to the appropriate scope.
class ReadySlotPromise
{
public:

   // Prepare the slot promise
   ReadySlotPromise( ConditionSlotPromise   && promise,
                     ConditionSlotIteration && slotIteration );

   // Signal it once the ConditionStore back to a consistent state
   void process();

private:

   ConditionSlotPromise m_promise;
   ConditionSlotIteration m_slotIteration;

};

// We will repeatedly manipulate lists of such ready promises, better have a shorthand notation for it
using ReadySlotPromiseList = std::vector< ReadySlotPromise >;

}


// Inline implementations
#include "detail/ConditionSlotPromises.ipp"

#endif
