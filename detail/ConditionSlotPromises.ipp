namespace detail
{

inline
ReadySlotPromise::ReadySlotPromise( ConditionSlotPromise   && promise,
                                    ConditionSlotIteration && slotIteration )
   : m_promise{ std::move(promise) }
   , m_slotIteration( std::move(slotIteration) )
{ }


inline
void
ReadySlotPromise::process()
{
   m_promise.set_value( std::move(m_slotIteration) );
}

}
