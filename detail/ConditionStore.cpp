#include "cpp_next/future.hpp"

#include "exceptions.hpp"

#include "detail/ConditionStore.hpp"


namespace detail
{

ConditionStore::ConditionStore( const size_t capacity )
   : m_slotKnowledge{ capacity }
   , m_constructionWaitQueues( capacity )
{ }


ConditionHandleImpl
ConditionStore::registerCondition( const framework::ConditionID & id,
                                   const OptionalConditionKind  & kind )
{
   // Make sure that versioned storage is setup for this condition, configure a handle with it
   auto & versionedCondition = m_slotKnowledge.registerCondition( id, kind );
   return ConditionHandleImpl{ *this, versionedCondition };
}


size_t
ConditionStore::availableSlots() const
{
   // Synchronize with slot allocation/liberation operations before performing this query
   std::lock_guard< std::mutex > lock{ m_globalMutex };
   return m_slotKnowledge.availableSlots();
}


ConditionSlotFuture
ConditionStore::allocateSlot( const framework::TimePoint & eventTimestamp )
{
   // Synchronize with concurrent operations affecting slot metadata (allocations, liberation, writes...)
   std::lock_guard< std::mutex > lock{ m_globalMutex };
   
   // The slot identifier of the newly allocated slot, if any, will be stored here
   ConditionSlotID slotID;
   
   // If we already have a suitable slot for this event around, we can reuse it
   if( m_slotKnowledge.reserveSuitableSlot( eventTimestamp, slotID ) )
   {
      if( m_slotKnowledge.slotFullyConstructed( slotID ) ) {
         return cpp_next::make_ready_future(
            ConditionSlotIteration{
               ConditionSlot{ slotID, *this },
               MissingConditions::NO_MISSING_CONDITION
            }
         );
      } else {
         m_constructionWaitQueues[slotID].emplace_back();
         return m_constructionWaitQueues[slotID].back().get_future();
      }
   }
   
   // If we are already building a new slot, but still loading its raw conditions, we cannot tell whether that
   // slot will be suitable or not for the current event, as we don't know its IoV, so we should wait for it
   if( m_slotKnowledge.rawConditionsMissing() ) {
      m_iovWaitQueue.emplace( eventTimestamp );
      return m_iovWaitQueue.back().promise.get_future();
   }
   
   // If we know that no suitable slot exists or is being created, we should try to create a new slot
   if( m_slotKnowledge.reserveUnusedSlot( slotID ) ) {
      auto missingConditions = m_slotKnowledge.setupSlot( slotID, eventTimestamp );
      return cpp_next::make_ready_future(
         ConditionSlotIteration{
            ConditionSlot{ slotID, *this },
            std::move(missingConditions)
         }
      );
   }
   
   // If there is no room for a new slot, we need to wait for one of the existing ones to free up
   m_liberationWaitQueue.emplace( eventTimestamp );
   return m_liberationWaitQueue.back().promise.get_future();
}


void
ConditionStore::processIoVWaitQueue( const ConditionSlotID        slotID,
                                           ReadySlotPromiseList & readyPromises )
{
   // We now know the final slot IoV, after all raw conditions have been inserted
   const framework::TimeInterval & finalIoV = m_slotKnowledge.slotIoV( slotID );
   
   // Process the slot allocation requests that were waiting for this information
   ConditionSlotID newSlotID;
   const std::size_t iovClients = m_iovWaitQueue.size();
   for( std::size_t i = 0; i < iovClients; ++i )
   {
      auto & request = m_iovWaitQueue.front();

      if( finalIoV.contains( request.eventTimestamp ) )
      {
         // The slot's IoV matches this allocation request, so both events can share it
         m_slotKnowledge.reserveSlot( slotID );
         m_constructionWaitQueues[slotID].push_back( std::move(request.promise) );
      }
      else if( m_slotKnowledge.rawConditionsMissing() )
      {
         // We need another slot, but raw condition IO has already resumed: go back to the IoV wait queue
         m_iovWaitQueue.push( std::move(request) );
      }
      else if( m_slotKnowledge.reserveUnusedSlot( newSlotID ) )
      {
         // We managed to reserve a new slot for this event, so this slot allocation request is fullfilled
         auto missingConditions = m_slotKnowledge.setupSlot( newSlotID, request.eventTimestamp );
         readyPromises.emplace_back(
            std::move(request.promise),
            ConditionSlotIteration{
               ConditionSlot{ newSlotID, *this },
               std::move(missingConditions)
            }
         );
      }
      else
      {
         // We need to allocate a new slot, but none is available: wait for one to free up
         m_liberationWaitQueue.push( std::move(request) );
      }

      m_iovWaitQueue.pop();
   }
}


void
ConditionStore::processConstructionWaitQueue( const ConditionSlotID        slotID,
                                                    ReadySlotPromiseList & readyPromises )
{
   for( auto & promise : m_constructionWaitQueues[slotID] )
   {
      readyPromises.emplace_back(
         std::move(promise),
         ConditionSlotIteration{
            ConditionSlot{ slotID, *this },
            MissingConditions::NO_MISSING_CONDITION
         }
      );
   }
   m_constructionWaitQueues[slotID].clear();
}


void
ConditionStore::processLiberationWaitQueue( const ConditionSlotID        slotID,
                                                  ReadySlotPromiseList & readyPromises )
{
   // If there were no pending slot allocation requests, we don't need to do anything
   if( m_liberationWaitQueue.empty() ) return;

   // Allocate the freshly liberated slot to the first pending requestor
   auto & firstRequest = m_liberationWaitQueue.front();
   m_slotKnowledge.reserveSlot( slotID );
   auto missingConditions = m_slotKnowledge.setupSlot( slotID, firstRequest.eventTimestamp );
   readyPromises.emplace_back(
      std::move(firstRequest.promise),
      ConditionSlotIteration{
         ConditionSlot{ slotID, *this },
         std::move(missingConditions)
      }
   );
   m_liberationWaitQueue.pop();
   
   // We should never simultaneously have liberation and IoV wait requests, because IoV wait
   // requests mean "a slot is being created, and I don't know whether it is suitable for me or not"
   // whereas liberation requests mean "I know that no slot being created is suitable for me".
   if( !m_iovWaitQueue.empty() ) throw PrototypeBugDetected{};
   
   // The remaining allocation requests will be processed once we know the IoV of the newly created slot.
   m_liberationWaitQueue.swap( m_iovWaitQueue );
}


ConditionStore::SlotIDRequest::SlotIDRequest( const framework::TimePoint & p_eventTimestamp )
   : eventTimestamp{ p_eventTimestamp }
{ }


void
ConditionStore::liberateSlot( const ConditionSlotID slotID )
{
   // Register that the slot has one less user, review slot allocation requests accordingly
   ReadySlotPromiseList readyPromises;
   {
      // Synchronize with other slot allocation and liberation operations
      std::lock_guard< std::mutex > lock{ m_globalMutex };
      
      // Take not that the slot has one less user, possibly liberating it for further use
      if( m_slotKnowledge.releaseSlot( slotID ) ) {
         processLiberationWaitQueue( slotID, readyPromises );
      }
   }
   
   // Process the slot allocation promises that we can now fulfill
   for( auto & promise : readyPromises ) {
      promise.process();
   }
}


std::unique_lock< std::mutex >
ConditionStore::acquireLock() const
{
   return std::unique_lock<std::mutex>{ m_globalMutex };
}


ReadySlotPromiseList
ConditionStore::notifyConditionInsertion( const ConditionSlotID           slotID,
                                          const ConditionKind             kind,
                                          const framework::TimeInterval & iov )
{
   // This operation may result in some condition slot promises being fulfilled, prepare to handle that
   ReadySlotPromiseList readyPromises;
   
   // Update condition metadata to match the newly inserted condition
   auto outcome = m_slotKnowledge.notifyConditionInsertion( slotID, kind, iov );

   // If this results in the slot's IoV being defined, process clients waiting for this event
   if( outcome.slotValidityAvailable ) {
      processIoVWaitQueue( slotID, readyPromises );
   }
   
   // If this reults in the slot being fully constructed, process clients waiting for this event
   if( outcome.slotFullyConstructed ) {
      processConstructionWaitQueue( slotID, readyPromises );
   }
   
   // Tell the ConditionHandle to fulfill our promises after releasing the lock
   return readyPromises;
}

}
