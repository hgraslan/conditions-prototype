#ifndef CONDITION_STORE_HPP
#define CONDITION_STORE_HPP

#include <mutex>
#include <queue>
#include <vector>

#include "ConditionKind.hpp"
#include "ConditionSlot.hpp"

#include "detail/ConditionHandleImpl.hpp"
#include "detail/ConditionSlotPromises.hpp"
#include "detail/ConditionSlotKnowledge.hpp"
#include "detail/ConditionStoreInternals.hpp"
#include "detail/OptionalConditionKind.hpp"

#include "framework/identifiers.hpp"
#include "framework/timing.hpp"


namespace detail
{

// A condition store manages a set of condition slots. It allocates slots to input events,
// disposes of them when they are not needed anymore, and gives access to them through handles.
class ConditionStore
{
public:

   // Initialize a condition store with "capacity" slots
   ConditionStore( const size_t capacity );
   
   // Register a (possibly new) condition, get a handle to its versioned storage
   ConditionHandleImpl registerCondition( const framework::ConditionID & id,
                                          const OptionalConditionKind  & kind );
   
   // Tell how many condition slots are available for use
   size_t availableSlots() const;
   
   // Request a condition slot, get a future that will be set once the slot is available
   // and tell which conditions are missing, if any.
   ConditionSlotFuture allocateSlot( const framework::TimePoint & eventTimestamp );
   

private:

   // === INTERNAL METHODS ===

   // After a slot's IoV has been fully computed, review the allocation requests that were waiting for it.
   // This may fulfill some condition slot promises that were made previously, if so these ready promises
   // will be appended to the "readyPromises" container that is passed by reference.
   void processIoVWaitQueue( const ConditionSlotID        slotID,
                                   ReadySlotPromiseList & readyPromises );

   // After a slot has been fully filled, prepare to notify the associated condition slot promises
   void processConstructionWaitQueue( const ConditionSlotID        slotID,
                                            ReadySlotPromiseList & readyPromises );

   // After a slot has been liberated, review pending slot allocation requests
   void processLiberationWaitQueue( const ConditionSlotID        slotID,
                                          ReadySlotPromiseList & readyPromises );
   

   // === SLOT MANAGEMENT STATE ===
   
   // This class manages concrete data and metadata about condition slots
   ConditionSlotKnowledge m_slotKnowledge;
   
   // These containers hold allocation requests which cannot be satisfied yet because although a slot of
   // suitable IoV has been detected and reserved, that slot is still in the process of being constructed
   SlotData< std::vector<ConditionSlotPromise> > m_constructionWaitQueues;
   
   // When we can't even tell which slot we should use, we need to memorize more request metadata
   struct SlotIDRequest
   {
      const framework::TimePoint eventTimestamp;
      ConditionSlotPromise promise;
      
      SlotIDRequest( const framework::TimePoint & eventTimestamp );
   };
   
   // Slot allocation requests will be processed on a FIFO basis
   using SlotIDRequestQueue = std::queue< SlotIDRequest >;
   
   // This queue holds allocation requests which cannot be satisfied because we cannot tell whether a
   // new slot must be built or not, as raw condition IO (and thus slot IoV determination) is ongoing
   SlotIDRequestQueue m_iovWaitQueue;
   
   // This queue holds allocation requests which cannot be satisfied because all condition slots are busy
   SlotIDRequestQueue m_liberationWaitQueue;
   
   
   // === THREAD SYNCHRONIZATION ===
   
   // This mutex should be locked to avoid exposing an inconsistent ConditionStore state to other threads.
   //
   // Here is a synchronization protocol overview:
   //    - You need a lock to query slot usage, otherwise an inconsistent slot usage state may be observed.
   //    - You need a lock to allocate and liberate slots and to write condition data, as these three
   //      operations access/mutate the same ConditionStore internal state.
   //    - You do not need a lock to setup handles, because this setup is carried out in serial code.
   //    - You do not need a lock to read condition data, because writers are scheduled before readers.
   //
   mutable std::mutex m_globalMutex;


   // === RAII CONDITIONSLOT INTERFACE ===

   // Allow a ConditionSlot to notify that we are done processing an event, enabling condition garbage collection.
   friend class ::ConditionSlot;
   void liberateSlot( const detail::ConditionSlotID slotID );
   
   
   // === CONDITION HANDLE INTERFACE ===
   
   // Only ConditionHandleImplementation, which is effectively a part of the ConditionStore implementation,
   // should be allowed to use this interface
   friend class ConditionHandleImpl;
   
   // Condition handles must acquire the ConditionStore's internal lock before writing data in, to avoid
   // entering a data race with the condition slot allocation process (which can read conditions)
   std::unique_lock< std::mutex > acquireLock() const;
   
   // In addition, they must notify the ConditionStore after the condition has been written, while still
   // holding the lock, so that all internal metadata may be updated accordingly. This may result in the
   // fulfillment of some slot allocation promises, to be processed by the handle after releasing the lock.
   ReadySlotPromiseList notifyConditionInsertion( const ConditionSlotID           slotID,
                                                  const ConditionKind             kind,
                                                  const framework::TimeInterval & iov );

};

}

#endif
