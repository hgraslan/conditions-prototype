#ifndef CONDITION_STORE_INTERNALS_HPP
#define CONDITION_STORE_INTERNALS_HPP

#include "detail/dynamic_array.hpp"
#include "detail/OptionalConditionKind.hpp"
#include "detail/SharedConditionData.hpp"


// To enable ConditionHandle implementation and avoid circular dependencies, some parts of the
// ConditionStore internals must be made public and extracted from the ConditionStore.hpp header.
//
namespace detail
{

// To avoid the many thread safety issues that come with growable containers,
// we want to use fixed-size arrays for slot-specific data storage.
template< typename T >
using SlotData = dynamic_array<T>;


// Keeping N slots in RAM means keeping at most N versions of each condition in RAM.
// The simplest way to do so, with a unique condition identifier, is to use a condition array of size N.
// We can afford to do so because we manipulate conditions through cheaply copyable references.
struct VersionedCondition
{
   OptionalConditionKind kind;
   SlotData< SharedConditionData > data;
   
   VersionedCondition( const OptionalConditionKind & kind,
                       const size_t                  capacity );
};

}

#endif
