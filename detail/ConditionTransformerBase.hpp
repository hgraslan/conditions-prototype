#ifndef CONDITION_TRANSFORMER_BASE_HPP
#define CONDITION_TRANSFORMER_BASE_HPP

#include <array>
#include <functional>
#include <tuple>

#include "cpp_next/utility.hpp"

#include "ConditionAlgBase.hpp"
#include "ConditionData.hpp"
#include "ConditionHandle.hpp"

#include "framework/identifiers.hpp"
#include "framework/timing.hpp"


// Forward declaration of other prototype components
class ConditionSvc;


// This class factors out the commonalities between ConditionTransformer and ConditionMultiTransformer
//
// Since it assumes that algorithms are only used to produce derived conditions, it is NOT
// available when IO inside of ConditionAlgs is enabled.
//
#ifndef ALLOW_IO_IN_ALGORITHMS

namespace detail
{

// Transformers and MultiTransformers have in common that they take a variadic list of arguments
template< typename Result,
          typename... Args >
class ConditionTransformerBase : public ConditionAlgBase
{
public:

   // The user implements the condition derivation logic, without needing to care about the IoV
   virtual Result operator()( const Args & ... args ) const = 0;


protected:

   // All condition transformers need to register to various management services and announce their input dependencies
   using ConditionIDRef = std::reference_wrapper< const framework::ConditionID >;
   using ArgsIDs = std::array< ConditionIDRef, sizeof...(Args) >;
   ConditionTransformerBase( ConditionSvc          &  conditionService,
                             framework::IScheduler &  scheduler,
                             ArgsIDs               && argsIDs );

   // We need a layer of indirection below the constructor in order to generate the index sequence that will be
   // used to register the algorithm's condition inputs
   using ArgsHandles = std::tuple< std::reference_wrapper< const ConditionReadHandle<Args> >... >;
   template< std::size_t... ArgIndexes >
   ArgsHandles registerArgs( ArgsIDs && argsIDs,
                             cpp_next::index_sequence<ArgIndexes...> );

   // Derive the output condition data and its IoV from the input arguments that we received
   ConditionData< Result > deriveOutput( const ConditionData<Args> & ... args ) const;

   // The input handles are stored here
   const ArgsHandles argsHandles;

};


// Since ConditionTransformer is intended for condition derivation, algorithms without inputs are not allowed.
template< typename Result >
class ConditionTransformerBase< Result >;

}


// Inline implementations
#include "ConditionTransformerBase.ipp"


#endif

#endif
