namespace detail
{

template< typename Result,
          typename... Args >
ConditionTransformerBase< Result, Args... >::ConditionTransformerBase( ConditionSvc          &  conditionService,
                                                                       framework::IScheduler &  scheduler,
                                                                       ArgsIDs               && argsIDs )
   : ConditionAlgBase{ conditionService, scheduler }
   , argsHandles{ registerArgs( std::move(argsIDs),
                                cpp_next::index_sequence_for<Args...>{} ) }
{ }


template< typename Result,
          typename... Args >
template< std::size_t... ArgsIndexes >
typename ConditionTransformerBase< Result, Args... >::ArgsHandles
ConditionTransformerBase< Result, Args... >::registerArgs( ArgsIDs && argsIDs,
                                                           cpp_next::index_sequence<ArgsIndexes...> )
{
   // Build the tuple of ConditionReadHandle references corresponding to the input arguments
   return ArgsHandles{ std::cref( ConditionAlgBase::registerInput<Args>( argsIDs[ArgsIndexes] ) )... };
}


template< typename Result,
          typename... Args >
ConditionData< Result >
ConditionTransformerBase< Result, Args... >::deriveOutput( const ConditionData<Args> & ... args ) const
{
   // The output is derived by applying the user-supplied functor to the arguments, and computing
   // the output IoV as the intersection of input IoVs.
   return ConditionData< Result >{
      (*this)( args.value... ),
      framework::TimeInterval::intersect( { args.iov... } )
   };
}

}
