#include "detail/MockConditionProducer.hpp"


namespace detail
{

bool
MockConditionProducer::outputMissing( const ConditionSlot & targetSlot ) const
{
   return m_impl->outputMissing( targetSlot );
}


void
MockConditionProducer::produceOutput( const framework::TimePoint & eventTimestamp,
                                      const ConditionSlot        & targetSlot ) const
{
   m_impl->produceOutput( eventTimestamp, targetSlot );
}

}
