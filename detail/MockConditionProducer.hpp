#ifndef MOCK_CONDITION_PRODUCER_HPP
#define MOCK_CONDITION_PRODUCER_HPP

#include <memory>

#include "ConditionHandle.hpp"
#include "ConditionSlot.hpp"

#include "detail/ConditionDataSet.hpp"

#include "framework/timing.hpp"


// As part of the implementation of MockConditionIOSvc, we need a class which is able to
// simulate IO by pushing condition data from a mock dataset to a condition slot.
namespace detail
{

class MockConditionProducer
{
public:

   // A MockConditionProducer can be constructed from a suitable write handle and mock dataset
   template< typename T >
   MockConditionProducer(       ConditionWriteHandle<T>     && writeHandle,
                          const detail::ConditionDataSet<T> &  mockDataSet );

   // Check whether the output condition is in transient storage or needs to be generated.
   bool outputMissing( const ConditionSlot & targetSlot ) const;

   // Insert the mock condition associated with one event into transient storage
   void produceOutput( const framework::TimePoint & eventTimestamp,
                       const ConditionSlot        & targetSlot ) const;


private:

   // To operate on multiple condition types, we need a type erasure layer, like a virtual interface
   class Interface
   {
   public:
   
      // Calls from the top-level MockConditionProducer interface are forwarded here
      virtual bool outputMissing( const ConditionSlot        & targetSlot ) const = 0;
      virtual void produceOutput( const framework::TimePoint & eventTimestamp,
                                  const ConditionSlot        & targetSlot ) const = 0;

   };

   // Beneath this interface, we put a concrete type-specific implementation
   template< typename T >
   struct Implementation : public Interface
   {
   public:

      // MockConditionProducer templated constructor calls go here
      Implementation(       ConditionWriteHandle<T>     && writeHandle,
                      const detail::ConditionDataSet<T> &  mockDataSet );

      // MockConditionProducer interface is implemented here
      bool outputMissing( const ConditionSlot & targetSlot ) const final override;
      void produceOutput( const framework::TimePoint & eventTimestamp,
                          const ConditionSlot        & targetSlot ) const final override;


   private:

      ConditionWriteHandle<T>     m_handle;
      detail::ConditionDataSet<T> m_dataSet;

   };

   // Once this is done, the type erasure work can easily be carried out by a unique_ptr
   std::unique_ptr<Interface> m_impl;

};

}


// Inline implementations
#include "detail/MockConditionProducer.ipp"

#endif
