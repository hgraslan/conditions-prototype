namespace detail
{

template< typename T >
MockConditionProducer::MockConditionProducer(       ConditionWriteHandle<T>     && writeHandle,
                                              const detail::ConditionDataSet<T> &  mockDataSet )
   : m_impl{ new Implementation<T>{ std::move(writeHandle), mockDataSet } }
{ }


template< typename T >
MockConditionProducer::Implementation<T>::Implementation(       ConditionWriteHandle<T>     && writeHandle,
                                                          const detail::ConditionDataSet<T> &  mockDataSet )
   : m_handle{ std::move(writeHandle) }
   , m_dataSet{ std::move(mockDataSet) }
{ }


template< typename T >
bool
MockConditionProducer::Implementation<T>::outputMissing( const ConditionSlot & targetSlot ) const
{
   return !m_handle.hasValue( targetSlot );
}


template< typename T >
void
MockConditionProducer::Implementation<T>::produceOutput( const framework::TimePoint & eventTimestamp,
                                                         const ConditionSlot        & targetSlot ) const
{
   // Access the condition data from the mock dataset
   const auto & conditionData = m_dataSet.find( eventTimestamp );
   
   // Put it into transient storage
   m_handle.put( targetSlot, conditionData );
}

}
