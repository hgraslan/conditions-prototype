#ifndef OPTIONAL_CONDITION_KIND_HPP
#define OPTIONAL_CONDITION_KIND_HPP

#include "cpp_next/optional.hpp"

#include "ConditionKind.hpp"


namespace detail
{

// Sometimes, we may or may not know about the condition kind, and can do without if we don't know
using OptionalConditionKind = cpp_next::optional< ConditionKind >;

}

#endif
