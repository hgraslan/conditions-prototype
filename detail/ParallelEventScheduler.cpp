#include <algorithm>

#include "detail/ParallelEventScheduler.hpp"


namespace detail
{

ParallelEventScheduler::ParallelEventScheduler(       ConditionSvc & conditionService,
                                                const std::size_t    threadPoolSize,
                                                const std::size_t    eventSlotAmount )
   : SchedulerBase( conditionService )
   , m_processingSlots( eventSlotAmount )
   , m_processingExecutor( threadPoolSize )
{
   std::generate(
      m_processingSlots.begin(),
      m_processingSlots.end(),
      []() -> cpp_next::future<void> {
         return cpp_next::make_ready_future();
      }
   );
}


void
ParallelEventScheduler::simulateEventLoop( const EventBatch & eventTimestamps )
{
   // Simulate a parallel event loop
   for( const auto & timestamp : eventTimestamps )
   {
      // Start condition slot setup (storage allocation & condition IO)
      auto conditionSlotFuture = SchedulerBase::setupConditions( timestamp );

      // Wait for an event processing slot to free up
      ProcessingSlot & processingSlot = waitForProcessingSlot();
      
      // Schedule event processing on that slot
      //
      // DEBUG: The C++ concurrency TS currently does not provide a way to ensure that a future
      //        continuation is scheduled on a finite thread pool, so we need to use a nonstandard
      //        Boost extension to the spec here, namely thread pool executors.
      //
      processingSlot = conditionSlotFuture.then(
         m_processingExecutor,
         [this, timestamp]( ConditionSlotFuture && slotFuture )
         {
            // Make sure that condition slot setup went well
            ConditionSlotIteration slotIteration = slotFuture.get();
            
            // Process the event in a sequential fashion
            processEvent(
               slotIteration
#ifdef ALLOW_IO_IN_ALGORITHMS
             , timestamp
#endif
            );
         }
      );
   }

   // Wait for all events in flight to be processed before returning control to the caller
   for( auto & processingSlot : m_processingSlots ) { processingSlot.get(); }
}


ParallelEventScheduler::ProcessingSlot &
ParallelEventScheduler::waitForProcessingSlot()
{
   // DEBUG: This is how we'd wait for an event processing slot to free up in the C++ Concurrency TS

   /* auto anySlotFuture = detail::when_any( m_processingSlots.begin(), m_processingSlots.end() );
   const std::size_t slotIndex = anySlotFuture.get().index;
   return m_processingSlots[ slotIndex ]; */

   // DEBUG: The following Boost-specific extension, however, is more appropriate for our use case
   //        and allows us to avoid the use of heavier shared_futures.
   
   return *boost::wait_for_any( m_processingSlots.begin(), m_processingSlots.end() );
}

}
