#ifndef PARALLEL_EVENT_SCHEDULER_HPP
#define PARALLEL_EVENT_SCHEDULER_HPP

#include <cstddef>
#include <thread>

#include "cpp_next/future.hpp"

#include "detail/dynamic_array.hpp"
#include "detail/executors.hpp"
#include "detail/SchedulerBase.hpp"


// Forward declaration of other framework components
class ConditionSvc;


// The following Gaudi scheduler mock simulates a sequential event loop
namespace detail
{

class ParallelEventScheduler : public SchedulerBase
{
public:

   // To allocate slots and manage I/O, a Scheduler needs access to the ConditionSvc.
   // In addition, this parallel scheduler manages a thread pool whose size must be specified.
   ParallelEventScheduler(       ConditionSvc & conditionService,
                           const std::size_t    threadPoolSize,
                           const std::size_t    eventSlotAmount );

   // This Scheduler mock processes events in parallel
   void simulateEventLoop( const EventBatch & eventTimestamps ) final override;


private:

   // Wait for an event processing slot to free up, reuse the associated future
   using ProcessingSlot = cpp_next::future<void>;
   ProcessingSlot & waitForProcessingSlot();

   // The futures associated with ongoing event processing tasks will go here
   detail::dynamic_array< ProcessingSlot > m_processingSlots;

   // The associated event processing tasks will be executed here
   detail::ThreadPoolExecutor m_processingExecutor;

};

}

#endif
