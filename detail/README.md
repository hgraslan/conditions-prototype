The classes and structures that appear within this directory and namespace
should be considered as implementation details of the prototype. They are only
intended for internal use by the condition processing infrastructure, and should
not be manipulated by clients.
