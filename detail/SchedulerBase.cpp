#include <unordered_set>

#include "ConditionSvc.hpp"

#include "detail/SchedulerBase.hpp"


namespace detail
{

SchedulerBase::SchedulerBase( ConditionSvc & conditionService )
   : m_conditionService( conditionService )
{ }


void
SchedulerBase::registerConditionAlg( const framework::IConditionAlg & alg )
{
   m_conditionAlgs.emplace_back( &alg );
}

}
