#ifndef SCHEDULER_BASE_HPP
#define SCHEDULER_BASE_HPP

#include <vector>

#include "framework/IConditionAlg.hpp"
#include "framework/IScheduler.hpp"
#include "framework/timing.hpp"


// Forward declaration of other framework components
class ConditionSvc;


// The following class is intended as a base class to all Gaudi scheduler mocks.
// It provides the basic scheduling logic that is shared by all mocks.
namespace detail
{

class SchedulerBase : public framework::IScheduler
{
public:

   // To allocate slots and manage I/O, a Scheduler needs access to the ConditionSvc
   SchedulerBase( ConditionSvc & conditionService );

   // We implement ConditionAlg registration at this level of the class hierarchy
   void registerConditionAlg( const framework::IConditionAlg & alg ) final override;


protected:

   // Asynchronously set up a condition slot for an event
   ConditionSlotFuture setupConditions( const framework::TimePoint & timestamp );

   // DEBUG: At the moment, the logic to process a single event sequentially is located here.
   //        Once algorithm-level scheduling is implemented, this will move elsewhere in the class hierarchy.
   void processEvent(
            ConditionSlotIteration & slotIteration
#ifdef ALLOW_IO_IN_ALGORITHMS
    , const framework::TimePoint   & timestamp
#endif
   ) const;


private:

   ConditionSvc & m_conditionService;
   using ConditionAlgPtr = const framework::IConditionAlg *;
   using ConditionAlgList = std::vector< ConditionAlgPtr >;
   ConditionAlgList m_conditionAlgs;

};

}


// Inline implementations
#include "detail/SchedulerBase.ipp"

#endif
