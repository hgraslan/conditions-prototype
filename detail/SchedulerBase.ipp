#include <unordered_set>

#include "ConditionSvc.hpp"


namespace detail
{

inline
ConditionSlotFuture
SchedulerBase::setupConditions( const framework::TimePoint & timestamp )
{
	return m_conditionService.setupConditions( timestamp );
}


inline
void
SchedulerBase::processEvent(
         ConditionSlotIteration & slotIteration
#ifdef ALLOW_IO_IN_ALGORITHMS
 , const framework::TimePoint   & timestamp
#endif
) const
{
   // DEBUG: Ignore the list of missing conditions that we received from the caller for now.
   //        In the future, we will use this information to implement more efficient ConditionAlg scheduling.
   const ConditionSlot & slot = slotIteration.slot;

   // Determine which condition algorithms need to run
   using ConditionAlgSet = std::unordered_set< ConditionAlgPtr >;
   ConditionAlgSet scheduledAlgs;
   std::copy_if(
      m_conditionAlgs.cbegin(),
      m_conditionAlgs.cend(),
      std::inserter( scheduledAlgs, scheduledAlgs.end() ),
      [&slot]( const ConditionAlgPtr alg ) -> bool {
         return alg->outputMissing( slot );
      }
   );

   // Brute-force the algorithm dependency graph until all scheduled algorithms have run
   // (Admittedly quite inelegant, but this mock is not trying to beat the Gaudi scheduler at its game)
   while( !scheduledAlgs.empty() )
   {
      // We need a way to detect scheduler stalls caused by circular dependencies.
      // These translate into no ConditionAlg managing to run although Algs still remain.
      bool stallDetected = true;
      
      // Try to run every scheduled algorithm
      for( auto algIt = scheduledAlgs.begin(); algIt != scheduledAlgs.end(); ) 
      {
         const auto & alg = **algIt;
         if( alg.inputsPresent( slot ) )
         {
            // This algorithm is ready to run, so run it and remove it from the scheduled set
            alg.execute(
               slot
#ifdef ALLOW_IO_IN_ALGORITHMS
             , timestamp
#endif
            );
            algIt = scheduledAlgs.erase( algIt );

            // If we managed to run at least one algorithm, no stall occured
            stallDetected = false;
         }
         else
         {
            // We cannot run this algorithm, skip it for now
            ++algIt;
         }
      }

      // If a scheduler stall was detected, we must report the bad news
      if( stallDetected ) throw SchedulerStallDetected{};
   }
   
   // DEBUG: At the end, all the derived conditions will have been generated
   slotIteration.missingConditions.derived.clear();
}

}
