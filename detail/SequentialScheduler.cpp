#include "detail/SequentialScheduler.hpp"


namespace detail
{

void
SequentialScheduler::simulateEventLoop( const EventBatch & eventTimestamps )
{
   // Simulate a serial event loop
   for( const auto & timestamp : eventTimestamps )
   {
      // Synchronously set up a condition slot, carrying out condition IO as necessary
      ConditionSlotFuture slotFuture = SchedulerBase::setupConditions( timestamp );
      ConditionSlotIteration slotIteration = slotFuture.get();

      // Process the event in a sequential fashion
      SchedulerBase::processEvent(
         slotIteration
#ifdef ALLOW_IO_IN_ALGORITHMS
       , timestamp
#endif
      );
   }
}

}
