#ifndef SEQUENTIAL_SCHEDULER_HPP
#define SEQUENTIAL_SCHEDULER_HPP

#include "detail/SchedulerBase.hpp"


// Forward declaration of other framework components
class ConditionSvc;


// The following Gaudi scheduler mock simulates a sequential event loop
namespace detail
{

class SequentialScheduler : public SchedulerBase
{
public:

   // To allocate slots and manage I/O, a Scheduler needs access to the ConditionSvc
   using SchedulerBase::SchedulerBase;

   // This Scheduler mock processes its event batch sequentially
   void simulateEventLoop( const EventBatch & eventTimestamps ) final override;

};

}

#endif
