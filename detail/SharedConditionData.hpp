#ifndef SHARED_CONDITION_DATA_HPP
#define SHARED_CONDITION_DATA_HPP

#include <memory>

#include "ConditionData.hpp"

#include "detail/AnyConditionData.hpp"

#include "framework/timing.hpp"


namespace detail
{

// Condition data is encapsulated into shared "blobs" to give it the following properties:
//    - Immutable (a blob's value cannot be changed after insertion)
//    - Type-erased (blobs are easy to store in containers)
//    - Shareable (we manipulate blobs through garbage-collected references)
//    - Nullable (we can express that a blob has not been generated yet)
class SharedConditionData
{
public:

   // Create an empty condition blob
   SharedConditionData();

   // Encapsulate raw condition data into a shareable blob
   template< typename T >
   SharedConditionData( ConditionData<T> && value );
   
   // Make a shareable copy of pre-existing condition data
   template< typename T >
   SharedConditionData( const ConditionData<T> & value );
   
   // Blobs are movable and copyable, with pointer-to-const semantics
   SharedConditionData( SharedConditionData && ) = default;
   SharedConditionData( const SharedConditionData & ) = default;
   SharedConditionData & operator=( SharedConditionData && ) = default;
   SharedConditionData & operator=( const SharedConditionData & ) = default;
   
   // Check whether the blob contains data
   bool exists() const;
   
   // Tell whether the blob contains condition data that is suitable for one event
   bool validFor( const framework::TimePoint & eventTimestamp ) const;
   
   // Access the IoV of the internal condition data. Will throw PrototypeBugDetected if
   // an empty blob is accessed in this way.
   const framework::TimeInterval & iov() const;
   
   // Access the internal condition data. Will throw PrototypeBugDetected if
   // an empty blob is dereferenced, and AnyConditionData::InvalidConditionType
   // if the wrong condition type is selected.
   template< typename T >
   const ConditionData<T> & get() const;
   
   // Discard the internal condition data
   void clear();
   

private:
   
   // Internal implementation
   std::shared_ptr< const AnyConditionData > m_impl;

};

}


// Inline implementations
#include "detail/SharedConditionData.ipp"

#endif
