#include "exceptions.hpp"


namespace detail
{

inline
SharedConditionData::SharedConditionData()
   : m_impl{ nullptr }
{ }


template< typename T >
SharedConditionData::SharedConditionData( ConditionData<T> && value )
   : m_impl{ std::make_shared< const AnyConditionData >( std::move(value) ) }
{ }


template< typename T >
SharedConditionData::SharedConditionData( const ConditionData<T> & value )
   : m_impl{ std::make_shared< const AnyConditionData >( value ) }
{ }


inline
bool
SharedConditionData::exists() const
{
	return bool( m_impl );
}


inline
bool
SharedConditionData::validFor( const framework::TimePoint & eventTimestamp ) const
{
   return m_impl && ( m_impl->iov().contains(eventTimestamp) );
}


inline
const framework::TimeInterval &
SharedConditionData::iov() const
{
	// Requesting the IoV of null condition data indicates a prototype bug
	if( !m_impl ) throw PrototypeBugDetected{};
	
	// If that doesn't happen, we can fetch it from the internal data
   return m_impl->iov();
}


template< typename T >
const ConditionData<T> &
SharedConditionData::get() const
{
	// Accessing null condition data indicates a prototype bug, because condition readers should
	// not be scheduled before the data is set.
	if( !m_impl ) throw PrototypeBugDetected{};
	
	// If that doesn't happen, we can try to access the internal data.
	return m_impl->template get<T>();
}


inline
void
SharedConditionData::clear()
{
	m_impl.reset();
}

}
