namespace detail
{

template< typename F,
          typename... ArgsToBind >
SingleUseBindWrapper<F, ArgsToBind...>::SingleUseBindWrapper( const FD & f )
   : m_f{ f }
{ }


template< typename F,
          typename... ArgsToBind >
SingleUseBindWrapper<F, ArgsToBind...>::SingleUseBindWrapper( FD && f )
   : m_f{ std::move(f) }
{ }


template< typename F,
          typename... ArgsToBind >
template< typename... ArgsFromBindImpl >
auto
SingleUseBindWrapper<F, ArgsToBind...>::operator()( ArgsFromBindImpl &&... args ) ->
   decltype( m_f( input_from_bind<ArgsToBind>::transform( std::forward<ArgsFromBindImpl>(args) )... ) )
{
   return m_f( input_from_bind<ArgsToBind>::transform( std::forward<ArgsFromBindImpl>(args) )... );
}

}
