#ifndef DYNAMIC_ARRAY_HPP
#define DYNAMIC_ARRAY_HPP

#include <vector>

#include "exceptions.hpp"


namespace detail
{

// This class implements an STL-ified equivalent of a C Variable Length Array:
// a container whose size is chosen at runtime, but remains fixed after creation.
template< typename T >
class dynamic_array
{
private:
   
   // Internally, dynamic_array is implemented as an STL vector
   using Implementation = std::vector<T>;

public:
   
   // STL typedefs are copied from the implementation
   using value_type = typename Implementation::value_type;
   using allocator_type = typename Implementation::allocator_type;
   using size_type = typename Implementation::size_type;
   using difference_type = typename Implementation::difference_type;
   using reference = typename Implementation::reference;
   using const_reference = typename Implementation::const_reference;
   using pointer = typename Implementation::pointer;
   using const_pointer = typename Implementation::const_pointer;
   using iterator = typename Implementation::iterator;
   using const_iterator = typename Implementation::const_iterator;
   using reverse_iterator = typename Implementation::reverse_iterator;
   using const_reverse_iterator = typename Implementation::const_reverse_iterator;
   
   // Construction semantics are similar to those of std::vector,
   // except for the fact that the size must be specified at creation time.
   //
   // Additionally, null arrays of size 0 are not supported, and using this array
   // size will lead to the InvalidSize exception being thrown.
   //
   explicit dynamic_array( size_type count );
   explicit dynamic_array( size_type count, const T& value );
   template< class InputIt > dynamic_array( InputIt first, InputIt last );
   dynamic_array( const dynamic_array& other );
   dynamic_array( dynamic_array&& other );
   dynamic_array( std::initializer_list<T> init );
   class InvalidSize : public ConditionPrototypeException{};
   
   // Assignment is only allowed between dynamic arrays of identical size.
   //
   // Trying to assign an array of a different size will lead to InvalidAssignment being thrown.
   //
   dynamic_array & operator=( const dynamic_array & other );
   dynamic_array & operator=( dynamic_array && other );
   dynamic_array & operator=( std::initializer_list<T> init );
   void assign( const T& value );
   template< class InputIt > void assign( InputIt first, InputIt last );
   void assign( std::initializer_list<T> ilist );
   class InvalidAssignment : public ConditionPrototypeException{};
   
   // The underlying allocator is available, for those few who care
   allocator_type get_allocator() const;
   
   // Element access follows the same rules as for std::vector
   reference at( size_type pos );
   const_reference at( size_type pos ) const;
   reference operator[]( size_type pos );
   const_reference operator[]( size_type pos ) const;
   reference front();
   const_reference front() const;
   reference back();
   const_reference back() const;
   T * data();
   const T * data() const;
   
   // Iterators also work like in std::vector
   iterator begin();
   const_iterator begin() const;
   const_iterator cbegin() const;
   iterator end();
   const_iterator end() const;
   const_iterator cend() const;
   reverse_iterator rbegin();
   const_reverse_iterator rbegin() const;
   const_reverse_iterator crbegin() const;
   reverse_iterator rend();
   const_reverse_iterator rend() const;
   const_reverse_iterator crend() const;
   
   // Capacity management is greatly simplified because the container size is nonzero and cannot vary
   size_type size() const;
   
   // Similarly, the only allowed modifer is swapping with another array of identical size
   void swap( dynamic_array & other );
   
      
private:

   // Vector implementation is stored here
   Implementation m_impl;
   
   // Initialization-time check that its size is non-null
   void check_impl_size() const;

};

}


// Inline implementations
#include "detail/dynamic_array.ipp"

#endif
