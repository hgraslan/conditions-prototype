namespace detail
{

template< typename T >
dynamic_array<T>::dynamic_array( size_type count )
   : m_impl( count )
{
   check_impl_size();
}


template< typename T >
dynamic_array<T>::dynamic_array( size_type count, const T& value )
   : m_impl( count, value )
{
   check_impl_size();
}


template< typename T >
template< class InputIt >
dynamic_array<T>::dynamic_array( InputIt first, InputIt last )
   : m_impl( first, last )
{
   check_impl_size();
}


template< typename T >
dynamic_array<T>::dynamic_array( const dynamic_array& other )
   : m_impl( other.m_impl )
{ }


template< typename T >
dynamic_array<T>::dynamic_array( dynamic_array&& other )
   : m_impl( std::move(other.m_impl) )
{ }


template< typename T >
dynamic_array<T>::dynamic_array( std::initializer_list<T> init )
   : m_impl( init )
{
   check_impl_size();
}


template< typename T >
void dynamic_array<T>::assign( const T& value )
{
   m_impl.assign( m_impl.size(), value );
}


template< typename T >
template< class InputIt >
void dynamic_array<T>::assign( InputIt first, InputIt last )
{
   std::vector<T> new_impl( first, last );
   if( new_impl.size() != m_impl.size() ) throw InvalidAssignment{};
   m_impl.swap( new_impl );
}


template< typename T >
void dynamic_array<T>::assign( std::initializer_list<T> ilist )
{
   if( m_impl.size() != ilist.size() ) throw InvalidAssignment{};
   m_impl.assign( ilist );
}


template< typename T >
typename dynamic_array<T>::allocator_type dynamic_array<T>::get_allocator() const
{
   return m_impl.get_allocator();
}


template< typename T >
typename dynamic_array<T>::reference dynamic_array<T>::at( size_type pos )
{
   return m_impl.at(pos);
}


template< typename T >
typename dynamic_array<T>::const_reference dynamic_array<T>::at( size_type pos ) const
{
   return m_impl.at(pos);
}


template< typename T >
typename dynamic_array<T>::reference dynamic_array<T>::operator[]( size_type pos )
{
   return m_impl[pos];
}


template< typename T >
typename dynamic_array<T>::const_reference dynamic_array<T>::operator[]( size_type pos ) const
{
   return m_impl[pos];
}


template< typename T >
typename dynamic_array<T>::reference dynamic_array<T>::front()
{
   return m_impl.front();
}


template< typename T >
typename dynamic_array<T>::const_reference dynamic_array<T>::front() const
{
   return m_impl.front();
}


template< typename T >
typename dynamic_array<T>::reference dynamic_array<T>::back()
{
   return m_impl.back();
}


template< typename T >
typename dynamic_array<T>::const_reference dynamic_array<T>::back() const
{
   return m_impl.back();
}


template< typename T >
T * dynamic_array<T>::data()
{
   return m_impl.data();
}


template< typename T >
const T * dynamic_array<T>::data() const
{
   return m_impl.data();
}


template< typename T >
typename dynamic_array<T>::iterator dynamic_array<T>::begin()
{
   return m_impl.begin();
}


template< typename T >
typename dynamic_array<T>::const_iterator dynamic_array<T>::begin() const
{
   return m_impl.begin();
}


template< typename T >
typename dynamic_array<T>::const_iterator dynamic_array<T>::cbegin() const
{
   return m_impl.cbegin();
}


template< typename T >
typename dynamic_array<T>::iterator dynamic_array<T>::end()
{
   return m_impl.end();
}


template< typename T >
typename dynamic_array<T>::const_iterator dynamic_array<T>::end() const
{
   return m_impl.end();
}


template< typename T >
typename dynamic_array<T>::const_iterator dynamic_array<T>::cend() const
{
   return m_impl.cend();
}


template< typename T >
typename dynamic_array<T>::reverse_iterator dynamic_array<T>::rbegin()
{
   return m_impl.rbegin();
}


template< typename T >
typename dynamic_array<T>::const_reverse_iterator dynamic_array<T>::rbegin() const
{
   return m_impl.rbegin();
}


template< typename T >
typename dynamic_array<T>::const_reverse_iterator dynamic_array<T>::crbegin() const
{
   return m_impl.crbegin();
}


template< typename T >
typename dynamic_array<T>::reverse_iterator dynamic_array<T>::rend()
{
   return m_impl.rend();
}


template< typename T >
typename dynamic_array<T>::const_reverse_iterator dynamic_array<T>::rend() const
{
   return m_impl.rend();
}


template< typename T >
typename dynamic_array<T>::const_reverse_iterator dynamic_array<T>::crend() const
{
   return m_impl.crend();
}


template< typename T >
typename dynamic_array<T>::size_type dynamic_array<T>::size() const
{
   return m_impl.size();
}


template< typename T >
void dynamic_array<T>::swap( dynamic_array & other )
{
   if( size() != other.size() ) throw InvalidAssignment{};
   m_impl.swap( other.m_impl );
}


template< typename T >
void dynamic_array<T>::check_impl_size() const
{
   if( m_impl.size() == 0 ) throw InvalidSize{};
}

}
