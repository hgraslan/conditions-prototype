#ifndef EXECUTORS_HPP
#define EXECUTORS_HPP

#include <boost/thread/executors/basic_thread_pool.hpp>
#include <boost/thread/executors/inline_executor.hpp>


// Although mostly fit for our purposes, the C++ Concurrency TS still has some shortcomings that will
// hopefully be corrected before final language integration.
//
// For example, there is currently no standard way to attach a continuation to a future in a
// manner that is guaranteed not to spawn extra OS threads, nor is there a way to bound the amount of
// threads that continuations can spread on in general.
//
// We hence need to leverage the fact that our C++ Concurrency TS implementation is Boost.Thread, which
// provides better facilities to deal with these problems.
//
namespace detail
{

   // Future continuations which should not spawn any extra threads should be run on this global inline executor
   extern boost::executors::inline_executor inlineContinuationExecutor;

   // Future continuations which should be run on a finite pool of thread should using the following executor
   // type, with an appropriate number of threads.
   using ThreadPoolExecutor = boost::executors::basic_thread_pool;

}

#endif
