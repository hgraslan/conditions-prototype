#ifndef FUTURE_UNION_HPP
#define FUTURE_UNION_HPP

#include <type_traits>
#include <vector>

#include "cpp_next/future.hpp"


// To implement when_all and when_any from the C++ Concurrency TS, we need to load their inputs futures into
// some shared state, by moving or copying them depending on their type. future_union implements this.
namespace detail
{

// Both the copy- and moved-based variants of future_union have some ideas in common
template< typename InputIt >
struct future_union_base
{

   // Here are some useful type definitions
   using InputFutureType = typename std::iterator_traits<InputIt>::value_type;
   using InputList = std::vector< InputFutureType >;

};


// To decide whether we need to copy or move, we need some metaprogramming hackery.
// This is a version that moves the input futures in. It will be called if we have no choice.
template< typename InputIt,
          typename Enable = void >
struct future_union : future_union_base<InputIt>
{

   // Our input futures will go there eventually
   using typename future_union_base<InputIt>::InputList;
   InputList m_inputs;

   // Move the input futures in
   future_union( InputIt first, InputIt last );

};


// If the input futures are copyable (i.e. they are shared_futures), we'll copy them instead.
template< typename InputIt >
struct future_union<
   InputIt,
   typename std::enable_if<
      std::is_copy_assignable<
         typename std::iterator_traits<InputIt>::value_type
      >::value
   >::type
> : future_union_base<InputIt>
{

   // Our input futures will go there eventually
   using typename future_union_base<InputIt>::InputList;
   InputList m_inputs;

   // Make a copy of the input shared_futures
   future_union( InputIt first, InputIt last );

};

}


// Inline implementations
#include "detail/future_union.ipp"

#endif
