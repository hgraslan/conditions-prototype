namespace detail
{

template< typename InputIt,
          typename Enable >
future_union<InputIt,Enable>::future_union( InputIt first, InputIt last )
{
   for( InputIt it = first; it < last; ++it )
   {
      m_inputs.emplace_back( std::move(*it) );
   }
}


template< typename InputIt >
future_union<
   InputIt,
   typename std::enable_if<
      std::is_copy_assignable<
         typename std::iterator_traits<InputIt>::value_type
      >::value
   >::type
>::future_union( InputIt first, InputIt last )
   : m_inputs( first, last )
{ }

}
