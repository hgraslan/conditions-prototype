#ifndef RUN_STATEMENT_PACK_HPP
#define RUN_STATEMENT_PACK_HPP


// Due to standard fuck-up, a C++11 parameter pack cannot expand into a sequence of statements. This will be fixed in
// C++17 with fold-expressions. In meantime, what we can do is hack through it using initializer lists.
#define DETAIL_RUN_STATEMENT_PACK( statement_pack ) \
   do { \
      const bool unused[] = { false, ( (void) (statement_pack), false )...  }; \
      (void) unused; \
   } while(0)


#endif
