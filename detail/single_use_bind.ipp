namespace detail
{

template< typename F,
          typename... ArgsToBind >
auto single_use_bind( F&& f, ArgsToBind&&... bindArgs ) ->
   decltype(
      std::bind(
         SingleUseBindWrapper< F, ArgsToBind... >{ std::forward<F>(f) },
         std::forward<ArgsToBind>(bindArgs)...
      )
   )
{
   return std::bind(
      SingleUseBindWrapper< F, ArgsToBind... >{ std::forward<F>(f) },
      std::forward<ArgsToBind>(bindArgs)...
   );
}

}
