#ifndef WHEN_ALL_HPP
#define WHEN_ALL_HPP

#include <type_traits>
#include <vector>

#include "cpp_next/future.hpp"


// Boost's implementation of when_all from the C++ Concurrency TS currently spawns a thread, which greatly
// reduces its usefulness as an asynchronous synchronization mechanism. This implementation doesn't.
//
namespace detail
{

// Only the iterator-based version is currently available
template< typename InputIt >
cpp_next::future< std::vector< typename std::iterator_traits<InputIt>::value_type > >
when_all( InputIt first, InputIt last );

}


// Inline implementations
#include "detail/when_all.ipp"

#endif
