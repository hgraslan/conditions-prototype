#include "detail/when_all_impl.hpp"

namespace detail
{

template< typename InputIt >
cpp_next::future< std::vector< typename std::iterator_traits<InputIt>::value_type > >
when_all( InputIt first, InputIt last )
{
   return when_all_impl<InputIt>( first, last );
}

}
