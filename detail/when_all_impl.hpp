#ifndef WHEN_ALL_IMPL_HPP
#define WHEN_ALL_IMPL_HPP

#include <atomic>
#include <cstddef>

#include "cpp_next/future.hpp"

#include "detail/future_union.hpp"


// This is the implementation of "when_all" from detail/when_all.hpp
namespace detail
{

// Internally, when_all uses a reference-counted shared state to track which futures have become ready
template< typename InputIt >
class when_all_shared_state : public future_union< InputIt >
{
public:

   // We'll need a shorthand to refer to our future_union base class
   using Base = future_union<InputIt>;

   // Load the input futures into the internal future_union
   when_all_shared_state( InputIt first, InputIt last );

   // Notify that an input future is ready
   void notifyReadyInput();

   // Notify that shared state initialization is over
   void notifyInitialization();
   
   // Get a future that will be set once all inputs are ready
   using Result = typename Base::InputList;
   cpp_next::future< Result > getFuture();


private:

   // Once all of our input futures are ready, we'll forward our input list to the output promise
   void setPromise();

   // This is the promise object associated with our output
   cpp_next::promise< Result > m_outputPromise;

   // This counter is initially set to the amount of input futures, plus one. It is decremented whenever an input future becomes ready,
   // and also when the shared state is fully initialized. The output promise will be set once this counter reaches zero.
   // This protocol ensures that the output promise will not be set until all inputs are ready AND we're done manipulating the shared state.
   std::atomic< std::size_t > m_remainingClients;
   
};


// Then we can implement when_all using this shared state
template< typename InputIt >
cpp_next::future< typename future_union_base<InputIt>::InputList >
when_all_impl( InputIt first, InputIt last );

}


// Inline implementations
#include "detail/when_all_impl.ipp"

#endif
