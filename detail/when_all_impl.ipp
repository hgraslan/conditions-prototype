#include <memory>

#include "detail/executors.hpp"


namespace detail
{

template< typename InputIt >
when_all_shared_state<InputIt>::when_all_shared_state( InputIt first, InputIt last )
   : Base{ first, last }
   , m_remainingClients{ Base::m_inputs.size() + 1 }
{
   // If we have no inputs, our output promise should be ready right away
   if( Base::m_inputs.empty() ) setPromise();
}


template< typename InputIt >
void
when_all_shared_state<InputIt>::notifyReadyInput()
{
   // Take note of the fact that an input future is ready
   std::size_t previousRemainingClients = m_remainingClients.fetch_sub(
      1ul,
      std::memory_order_relaxed
   );
   
   // If this was the last input future we were waiting for, and the shared state is fully initialized,
   // synchronize with the shared state initialization process and set the output promise.
   if( previousRemainingClients == 1ul ) {
      std::atomic_thread_fence( std::memory_order_acquire );
      setPromise();
   }
}


template< typename InputIt >
void
when_all_shared_state<InputIt>::notifyInitialization()
{
   // Take note of the fact that shared state initialization is ready, and make sure that all associated writes are visible to future continuations
   std::size_t previousRemainingClients = m_remainingClients.fetch_sub(
      1ul,
      std::memory_order_release
   );
   
   // If all input futures turn out to be ready, set the output promise
   if( previousRemainingClients == 1ul ) setPromise();
}


template< typename InputIt >
cpp_next::future< typename when_all_shared_state<InputIt>::Result >
when_all_shared_state<InputIt>::getFuture()
{
   return m_outputPromise.get_future();
}


template< typename InputIt >
void
when_all_shared_state<InputIt>::setPromise()
{
   // Forward our inputs to the output promise
   m_outputPromise.set_value( std::move(Base::m_inputs) );
}


template< typename InputIt >
cpp_next::future< typename future_union_base<InputIt>::InputList >
when_all_impl( InputIt first, InputIt last )
{
   // First, let's define some clearer names for the types that we're going to manipulate
   using FutureType = typename future_union_base<InputIt>::InputFutureType;
   using ValueType = typename FutureType::value_type;

   // Create our shared state
   auto sharedState = std::make_shared< when_all_shared_state<InputIt> >( first, last );

   // Associate each of our input futures with a continuation that updates the shared state.
   for( auto & input : sharedState->m_inputs )
   {
      // Unwrap the continuation's future to make this operation transparent to the client
      input = FutureType{
         input.then(
            // Run this continuation in the thread that set the input future
            detail::inlineContinuationExecutor,
            [sharedState]( FutureType && future ) -> ValueType {
               // Notify the shared state that one of its input futures has become ready
               sharedState->notifyReadyInput();

               // Forward the result of the input future
               return future.get();
            }
         )
      };
   }

   // Notify the shared state that it is now fully initialized, allowing the output promise to be set
   sharedState->notifyInitialization();
   
   // Provide the future associated with our internal promise
   return sharedState->getFuture();
}

}
