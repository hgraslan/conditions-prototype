#ifndef WHEN_ALL_HPP
#define WHEN_ALL_HPP

#include <type_traits>
#include <vector>

#include "cpp_next/future.hpp"

#include "detail/when_any_impl.hpp"


// Boost's implementation of when_any from the C++ Concurrency TS currently spawns a thread, which greatly
// reduces its usefulness as an asynchronous synchronization mechanism. This implementation doesn't.
//
// In addition, the interface of this implementation of when_any conforms better to the TS by providing an
// iterator to the future that became ready in its return type.
//
namespace detail
{

// Only the iterator-based version is currently available
template< typename InputIt >
cpp_next::future< when_any_result< std::vector< typename std::iterator_traits<InputIt>::value_type > > >
when_all( InputIt first, InputIt last );

}


// Inline implementations
#include "detail/when_any.ipp"

#endif
