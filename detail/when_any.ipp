namespace detail
{

template< typename InputIt >
cpp_next::future< when_any_result< std::vector< typename std::iterator_traits<InputIt>::value_type > > >
when_any( InputIt first, InputIt last )
{
   return when_any_impl<InputIt>( first, last );
}

}
