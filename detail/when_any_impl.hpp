#ifndef WHEN_ALL_IMPL_HPP
#define WHEN_ALL_IMPL_HPP

#include <atomic>
#include <cstddef>

#include "cpp_next/future.hpp"

#include "detail/future_union.hpp"


// This is the implementation of "when_any" from detail/when_any.hpp
namespace detail
{

// Definition of the output of when_any, straight from the C++ Concurrency TS
template < class Sequence >
struct when_any_result
{
    std::size_t index;
    Sequence futures;
};


// Internally, when_any uses a reference-counted shared state to track which futures have become ready
template< typename InputIt >
struct when_any_shared_state : future_union< InputIt >
{
public:

   // We'll need a shorthand to refer to our future_union base class
   using Base = future_union<InputIt>;

   // Load the input futures into the internal future_union
   when_any_shared_state( InputIt first, InputIt last );

   // Notify that an input future is ready
   void notifyReadyInput( std::size_t index );

   // Notify that shared state initialization is over
   void notifyInitialization();
   
   // Get a future that will be set once one of the inputs is ready
   using Result = when_any_result< typename Base::InputList >;
   cpp_next::future< Result > getFuture();


private:

   // When one of our inputs becomes ready, we'll forward that to our output promise
   void setPromise( std::size_t index );
   
   // This is the promise object associated with our output
   cpp_next::promise< Result > m_outputPromise;

   // This atomic variable will initially be set to an invalid input index, and the first ready input future will set it to its own index
   static const std::size_t INVALID_INDEX = std::size_t(-1);
   std::atomic< std::size_t > m_firstReadyInput;

   // This atomic counter will initially be set to 2, and reach zero once an input future is ready and the shared state is fully initialized
   std::atomic< int > m_remainingClients;
   
};


// Then we can implement when_any using this shared state
template< typename InputIt >
cpp_next::future< when_any_result< typename future_union_base<InputIt>::InputList > >
when_any_impl( InputIt first, InputIt last );

}


// Inline implementations
#include "detail/when_any_impl.ipp"

#endif
