#include <memory>

#include "detail/executors.hpp"


namespace detail
{

template< typename InputIt >
when_any_shared_state<InputIt>::when_any_shared_state( InputIt first, InputIt last )
   : Base{ first, last }
   , m_firstReadyInput{ INVALID_INDEX }
   , m_remainingClients{ 2 }
{
   // If we have no inputs, our output promise should be ready right away
   if( Base::m_inputs.empty() ) setPromise( INVALID_INDEX );
}


template< typename InputIt >
void
when_any_shared_state<InputIt>::notifyReadyInput( std::size_t index )
{
   // Attempt to register ourselves as the first input future that became ready
   std::size_t previousIndex = INVALID_INDEX;
   bool isFirstInput = m_firstReadyInput.compare_exchange_strong(
      previousIndex,
      index,
      std::memory_order_relaxed,
      std::memory_order_relaxed
   );
   
   // Stop here if another input future became ready before
   if( !isFirstInput ) return;

   // Take note of the fact that an input future is ready, with the following synchronization constraints:
   //    - Propagate the value of m_firstReadyInput to the shared state initialization task (in case *it* needs to set the promise)
   //    - Synchronize with the shared state initialization process (in case *we* need to set the promise)
   int previousRemainingClients = m_remainingClients.fetch_sub(
      1ul,
      std::memory_order_acq_rel
   );
   
   // If the shared state is already initialized, we can set the output promise to our index
   if( previousRemainingClients == 1ul ) setPromise( index );
}


template< typename InputIt >
void
when_any_shared_state<InputIt>::notifyInitialization()
{
   // Take note of the fact that shared state initialization is over, with the following synchronization constraints:
   //    - Propagate the final shared state to the input futures (in case *they* need to set the promise)
   //    - Synchronize with writes to m_firstReadyInput from input future continuations (in case *we* need to set the promise)
   int previousRemainingClients = m_remainingClients.fetch_sub(
      1ul,
      std::memory_order_acq_rel
   );
   
   // If one input future was already set, we can set the output promise to its index (whose value we just synchronized with)
   if( previousRemainingClients == 1ul ) setPromise( m_firstReadyInput.load( std::memory_order_relaxed ) );
}


template< typename InputIt >
cpp_next::future< typename when_any_shared_state<InputIt>::Result >
when_any_shared_state<InputIt>::getFuture()
{
   return m_outputPromise.get_future();
}


template< typename InputIt >
void
when_any_shared_state<InputIt>::setPromise( std::size_t index )
{
   // Forward our inputs to the output promise
   m_outputPromise.set_value( { index, std::move(Base::m_inputs) } );
}


template< typename InputIt >
cpp_next::future< when_any_result< typename future_union_base<InputIt>::InputList > >
when_any_impl( InputIt first, InputIt last )
{
   // First, let's define some clearer names for the types that we're going to manipulate
   using FutureType = typename future_union_base<InputIt>::InputFutureType;
   using ValueType = typename FutureType::value_type;

   // Create our shared state
   auto sharedState = std::make_shared< when_any_shared_state<InputIt> >( first, last );

   // Associate each of our input futures with a continuation that updates the shared state.
   const std::size_t input_size = sharedState->m_inputs.size();
   for( std::size_t i = 0; i < input_size; ++i )
   {
      // Unwrap the continuation's future to make this operation transparent to the client
      auto & input = sharedState->m_inputs[i];
      input = FutureType{
         input.then(
            // Run this continuation in the thread that set the input future
            detail::inlineContinuationExecutor,
            [i, sharedState]( FutureType && future ) -> ValueType {
               // Notify the shared state that this input future has become ready
               sharedState->notifyReadyInput(i);

               // Forward the result of the future
               return future.get();
            }
         )
      };
   }

   // Notify the shared state that it is now fully initialized, allowing the output promise to be set
   sharedState->notifyInitialization();
   
   // Provide the future associated with our internal promise
   return sharedState->getFuture();
}

}
