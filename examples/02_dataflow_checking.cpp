#include <functional>

#include "ConditionHandle.hpp"
#include "ConditionKind.hpp"
#include "exceptions.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "framework/ConditionDataflow.hpp"
#include "framework/identifiers.hpp"

#include "examples/examples_shared.hpp"


// This example illustrates the ability of the proposed condition infrastructure to detect and report
// errors in the user-specified condition dataflow.


// Every error that we detect and report will be isolated using the following simple test harness:
template< typename ExpectedException >
void detectError( const std::function< void( TransientConditionStorageSvc & ) > & buggyCode )
{
   try {
      TransientConditionStorageSvc transientStore{ 1 };
      buggyCode( transientStore );
      examples_shared::fail();
   } catch( const ExpectedException & ) { }
}


// Here is the error-checking that we currently perform
int main()
{
   examples_shared::title( "DATAFLOW ERROR CHECKING" );

   // Parameters of this usage example are defined here
   const framework::ConditionID conditionID{ examples_shared::generateConditionID() };
   const framework::ConditionUserID producerID{ examples_shared::generateConditionUserID() };
   const framework::ConditionUserID consumerID{ examples_shared::generateConditionUserID() };

   // We make sure that the condition dataflow graph is consistent and type-safe
   examples_shared::print( "Detecting inconsistent condition dataflow..." );
   detectError< InconsistentConditionType >(
      [&]( TransientConditionStorageSvc & transientStore ) {
         const auto firstWriteHandle = transientStore.registerOutput< std::string >(
            producerID,
            conditionID,
            ConditionKind::RAW
         );
         const auto firstReadHandle = transientStore.registerInput< int >(
            consumerID,
            conditionID
         );
      }
   );
   
   // We make sure that each condition has a producer by the time the dataflow graph is queried
   examples_shared::print( "Detecting a missing producer..." );
   detectError< framework::ConditionDataflow::ConditionLacksProducer >(
      [&]( TransientConditionStorageSvc & transientStore ) {
         const auto firstReadHandle = transientStore.registerInput< char >(
            consumerID,
            conditionID
         );
         transientStore.getConditionDataflow();
      }
   );
   
   // To avoid data races, we make sure that each condition only has one designated producer
   examples_shared::print( "Detecting multiple producers..." );
   detectError< ProducerAlreadyRegistered >(
      [&]( TransientConditionStorageSvc & transientStore ) {
         const auto firstWriteHandle = transientStore.registerOutput< float >(
            producerID,
            conditionID,
            ConditionKind::RAW
         );
         const auto secondWriteHandle = transientStore.registerOutput< float >(
            producerID,
            conditionID,
            ConditionKind::RAW
         );
      }
   );
   

   // And to avoid the inefficient scenario where conditions are produced, but never used,
   // we also make sure that each condition has a consumer by the time the dataflow graph is queried.
   examples_shared::print( "Detecting a missing consumer..." );
   detectError< framework::ConditionDataflow::ConditionLacksConsumers >(
      [&]( TransientConditionStorageSvc & transientStore ) {
         const auto firstWriteHandle = transientStore.registerOutput< double >(
            producerID,
            conditionID,
            ConditionKind::RAW
         );
         transientStore.getConditionDataflow();
      }
   );
   
   // Visually separate the output of this example from the next
   examples_shared::exampleSeparator();
   
   return 0;
}
