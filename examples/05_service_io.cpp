#include <algorithm>
#include <chrono>

#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionSlot.hpp"
#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/ConditionDataSet.hpp"

#include "framework/identifiers.hpp"
#include "framework/timing.hpp"

#include "examples/examples_shared.hpp"
#include "examples/MockIOSvc.hpp"


// This examples illustrates how the proposed infrastructure can carry out condition IO asynchronously
int main()
{
   examples_shared::title( "SERVICE-BASED CONDITION IO" );
   
   // Parameters of this usage example are defined here
   using MockConditionDataSet = std::vector< ConditionData< std::string > >;
   const framework::ConditionID dbConditionID{ examples_shared::generateConditionID() };
   const framework::ConditionID fileConditionID{ examples_shared::generateConditionID() };
   const framework::ConditionUserID exampleUserID{ examples_shared::generateConditionUserID() };
   const std::chrono::milliseconds dbLatency{ 4000 };
   const std::chrono::milliseconds fileLatency{ 1000 };

   // The following objects will be used when studying the simulated IO latency
   examples_shared::Timer timer;
   const auto minLatency = std::min( dbLatency, fileLatency );
   const auto maxLatency = std::max( dbLatency, fileLatency );
   
   // Setup the condition infrastructure for a storage capacity of one condition slot
   examples_shared::print( "Initializing the condition infrastructure..." );
   const std::size_t conditionSlotAmount{ 1 };
   TransientConditionStorageSvc transientStore{ conditionSlotAmount };
   ConditionSvc conditionService{ transientStore };
   
   // Add a simulation of database IO
   examples_shared::print( "Setting up a database IO simulation..." );
   MockIOSvc dbService{
      conditionService,
      { dbConditionID },
      dbLatency
   };
   dbService.setMockOutput< std::string >(
      dbConditionID,
      MockConditionDataSet {
         { "first DB blob", { 0, 3 } },
         { "second DB blob", { 5, 8 } }
      }
   );

   // Add a simulation of file IO
   examples_shared::print( "Setting up a file IO simulation..." );
   MockIOSvc fileService{
      conditionService,
      { fileConditionID },
      fileLatency
   };
   fileService.setMockOutput< std::string >(
      fileConditionID,
      MockConditionDataSet{
         { "first file blob", { 1, 5 } },
         { "second file blob", { 6, 8 } }
      }
   );
   
   // Add some readout stage so that we can check out what these services are writing
   examples_shared::print( "Setting up condition readout..." );
   const auto dbReadHandle = transientStore.registerInput< std::string >(
      exampleUserID,
      dbConditionID
   );
   const auto fileReadHandle = transientStore.registerInput< std::string >(
      exampleUserID,
      fileConditionID
   );
   
   // Report the framework's view of the condition dataflow
   const auto dataflowMetadata = transientStore.getConditionDataflow();
   examples_shared::printDataflowMetadata( dataflowMetadata );

   // To illustrate how the condition IO infrastructure works, we benchmark its performance in various cases
   const auto benchmark = [ &conditionService, &timer, &dbReadHandle, &fileReadHandle ](
      const framework::TimePoint         & eventTimestamp,
      const std::string               & scenarioName,
      const std::chrono::milliseconds & latencyUpperBound
   ){
      // Separate this output from that of the other benchmarks
      examples_shared::newline();
      
      // Request a condition slot for the proposed event
      examples_shared::print( "Starting condition slot allocation for timestamp "
                              + std::to_string( eventTimestamp ) + "..." );
      ConditionSlotFuture slotFuture = conditionService.setupConditions( eventTimestamp );
      
      // Measure the time it takes for the slot to become ready
      examples_shared::print( "Waiting for condition IO..." );
      timer.start();
      const ConditionSlot slot = slotFuture.get().slot;
      const auto ioDuration = timer.elapsed();

      // Make sure that the conditions were checked under the expected time constraints
      examples_shared::print( scenarioName + " conditions were fetched in "
                              + std::to_string(ioDuration.count()) + " ms" );
      examples_shared::check( ioDuration < latencyUpperBound );

      // Check out the condition data that we retrieved
      const ConditionData< std::string > & dbCondition = dbReadHandle.get( slot );
      const ConditionData< std::string > & fileCondition = fileReadHandle.get( slot );
      examples_shared::print( "Using conditions \"" + dbCondition.value + "\" and \"" + fileCondition.value + '"' );
   };
   
   // Case 1: No conditions are present, every IO must be run. But since the IO is run in parallel, we are
   //         bound by the latency of the slowest link, not the sum of the latencies of all links
   benchmark( 1, "Full", maxLatency + minLatency/2 );
   
   // Case 2: All conditions are cached, so no IO is run. Conditions are retrieved (nearly-)instantaneously.
   benchmark( 2, "Cached", minLatency/2 );
   
   // Case 3: DB conditions are invalidated and must be re-fetched. File conditions are left untouched.
   benchmark( 5, "DB", dbLatency + fileLatency/2 );

   // Case 4: File conditions are invalidated and must be re-fetched. DB conditions are left untouched.
   benchmark( 6, "File", fileLatency + dbLatency/2 );
   
   // Visually separate the output of this example from the next
   examples_shared::exampleSeparator();   

   return 0;
}
