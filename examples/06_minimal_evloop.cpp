#include <cstddef>
#include <vector>

#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/ConditionDataSet.hpp"
#include "detail/SequentialScheduler.hpp"

#include "framework/ConditionDataflow.hpp"
#include "framework/identifiers.hpp"
#include "framework/timing.hpp"

#include "examples/examples_shared.hpp"
#include "examples/MockIOSvc.hpp"


// This is a minimal example of an event loop simulation. In this example, conditions never change
// and are never read. This best-case scenario can be used to measure the minimal event scheduling overhead.

// We'll do the measurement for 1 and N conditions, which requires a layer of indirection.
void benchmark( const std::size_t conditionCount )
{
   // Parameters of this usage example are defined here
   using ConditionType = int;
   using MockConditionDataSet = std::vector< ConditionData< ConditionType > >;
   const std::chrono::milliseconds ioLatency{ 0 };
   const std::size_t eventCount{ examples_shared::empty_loop_params::eventCount };
   const std::size_t conditionSlotAmount{ 1 };
   
   // The following objects will be used for benchmarking
   examples_shared::Timer timer;
   
   // Setup the condition infrastructure
   examples_shared::print( "Initializing the condition infrastructure..." );
   TransientConditionStorageSvc transientStore{ conditionSlotAmount };
   ConditionSvc conditionService{ transientStore };
   detail::SequentialScheduler scheduler{ conditionService };

   // Prepare a batch of event timestamps for the scheduler to process
   examples_shared::print( "Generating an event batch..." );
   const auto eventBatch = examples_shared::generateEventBatch( eventCount );

   // Generate IDs for a bunch of raw conditions
   examples_shared::print( "Generating condition IDs..." );
   const auto rawConditionIDs = examples_shared::generateConditionIDs( conditionCount );

   // Prepare to simulate the IO of these conditions
   examples_shared::print( "Setting up an IO simulation..." );
   MockIOSvc ioService{
      conditionService,
      rawConditionIDs,
      ioLatency
   };
   ioService.setMultipleMockOutputs< ConditionType >(
      rawConditionIDs,
      MockConditionDataSet{
         { 0, framework::TimeInterval::INFINITE_INTERVAL }
      }
   );

   // Complete the condition processing pipeline with a readout stage
   examples_shared::print( "Setting up condition readout..." );
   const auto readHandles = examples_shared::setupConditionReaders< ConditionType >(
      conditionService,
      rawConditionIDs
   );

   // Report the framework's view of the condition dataflow
   const auto dataflowMetadata = transientStore.getConditionDataflow();
   examples_shared::printDataflowMetadata( dataflowMetadata );
   
   // Run the event loop simulation
   examples_shared::print( "Running the simulated event loop..." );
   timer.start();
   scheduler.simulateEventLoop( eventBatch );
   const auto eventLoopDuration = timer.elapsed();

   // Display the simulation details
   const auto totalMiliseconds = eventLoopDuration.count();
   const int microsecondsPerEvent = float(totalMiliseconds) * 1000 / eventCount;
   examples_shared::print( "Simulating "
                           + std::to_string( eventCount ) + " events took "
                           + std::to_string( totalMiliseconds ) + " ms (~"
                           + std::to_string( microsecondsPerEvent ) + " µs/event)" );
}


// Top-level benchmark logic goes here
int main()
{
   examples_shared::title( "MINIMAL EVENT LOOP BENCHMARK (IO only, conditions never change)" );
   
   benchmark( 1 );
   examples_shared::newline();
   benchmark( examples_shared::empty_loop_params::conditionCount );
   
   examples_shared::exampleSeparator();
   
   return 0;
}
