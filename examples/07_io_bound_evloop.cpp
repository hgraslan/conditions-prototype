#include <cstddef>

#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/ConditionDataSet.hpp"
#include "detail/SequentialScheduler.hpp"

#include "framework/ConditionDataflow.hpp"
#include "framework/identifiers.hpp"
#include "framework/timing.hpp"

#include "examples/BenchmarkIOSvc.hpp"
#include "examples/examples_shared.hpp"


// In this variant of the minimal event loop simulation, raw conditions now change for every event.
// This allows measuring the intrinsic overhead of condition IO.

// We'll do the measurement for 1 and N conditions, which requires a layer of indirection.
void benchmark( const std::size_t conditionCount )
{
   // Parameters of this usage example are defined here
   using ConditionType = int;
   const std::size_t eventCount{ examples_shared::empty_loop_params::eventCount };
   const std::size_t conditionSlotAmount{ 1 };
   
   // The following objects will be used for benchmarking
   examples_shared::Timer timer;
   
   // Setup the condition infrastructure
   examples_shared::print( "Initializing the condition infrastructure..." );
   TransientConditionStorageSvc transientStore{ conditionSlotAmount };
   ConditionSvc conditionService{ transientStore };
   detail::SequentialScheduler scheduler{ conditionService };

   // Prepare a batch of event timestamps for the scheduler to process
   examples_shared::print( "Generating an event batch..." );
   const auto eventBatch = examples_shared::generateEventBatch( eventCount );

   // Generate IDs for a bunch of raw conditions
   examples_shared::print( "Generating condition IDs..." );
   const auto rawConditionIDs = examples_shared::generateConditionIDs( conditionCount );

   // Prepare to simulate the IO of these conditions
   examples_shared::print( "Setting up an IO simulation..." );
   BenchmarkIOSvc ioService{
      conditionService,
      rawConditionIDs
   };

   // Complete the condition processing pipeline with a readout stage
   examples_shared::print( "Setting up condition readout..." );
   const auto readHandles = examples_shared::setupConditionReaders< ConditionType >(
      conditionService,
      rawConditionIDs
   );

   // Report the framework's view of the condition dataflow
   const auto dataflowMetadata = transientStore.getConditionDataflow();
   examples_shared::printDataflowMetadata( dataflowMetadata );
   
   // Run the event loop simulation
   examples_shared::print( "Running the simulated event loop..." );
   timer.start();
   scheduler.simulateEventLoop( eventBatch );
   const auto eventLoopDuration = timer.elapsed();

   // Display the simulation details
   const auto totalMiliseconds = eventLoopDuration.count();
   const int microsecondsPerEvent = float(totalMiliseconds) * 1000 / eventCount;
   examples_shared::print( "Simulating "
                           + std::to_string( eventCount ) + " events took "
                           + std::to_string( totalMiliseconds ) + " ms (~"
                           + std::to_string( microsecondsPerEvent ) + " µs/event)" );
}

int main()
{
   examples_shared::title( "IO-BOUND EVENT LOOP BENCHMARK (IO only, conditions change on every event)" );
   
   benchmark( 1 );
   examples_shared::newline();
   benchmark( examples_shared::empty_loop_params::conditionCount );
   
   examples_shared::exampleSeparator();
   
   return 0;
}
