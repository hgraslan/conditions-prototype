#include <cstddef>

#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/ConditionDataSet.hpp"
#include "detail/SequentialScheduler.hpp"

#include "framework/ConditionDataflow.hpp"
#include "framework/identifiers.hpp"
#include "framework/timing.hpp"

#include "examples/BenchmarkIOSvc.hpp"
#include "examples/examples_shared.hpp"
#include "examples/PassthroughAlg.hpp"


// This is a variant of the io_bound_evloop example with an extra pass-through condition algorithm.
// This example can be used to estimate the intrinsic overhead of ConditionAlgs.
// Since it relies on ConditionTransformer, it requires algorithm-based IO to be disabled.

// We'll do the measurement for 1 and N conditions, which requires a layer of indirection.
#ifndef ALLOW_IO_IN_ALGORITHMS
void benchmark( const std::size_t conditionCount )
{
   // Parameters of this usage example are defined here
   using ConditionType = int;
   const std::size_t conditionSlotAmount{ 1 };
   const std::size_t eventCount{ examples_shared::empty_loop_params::eventCount };
   
   // The following objects will be used for benchmarking
   examples_shared::Timer timer;
   
   // Setup the condition infrastructure
   examples_shared::print( "Initializing the condition infrastructure..." );
   TransientConditionStorageSvc transientStore{ conditionSlotAmount };
   ConditionSvc conditionService{ transientStore };
   detail::SequentialScheduler scheduler{ conditionService };

   // Prepare a batch of event timestamps for the scheduler to process
   examples_shared::print( "Generating an event batch..." );
   const auto eventBatch = examples_shared::generateEventBatch( eventCount );

   // Generate IDs for a bunch of raw conditions and associated derived conditions
   examples_shared::print( "Generating condition IDs..." );
   const auto rawConditionIDs = examples_shared::generateConditionIDs( conditionCount );
   const auto derivedConditionIDs = examples_shared::generateConditionIDs( conditionCount );

   // Prepare to simulate the IO of these conditions
   examples_shared::print( "Setting up an IO simulation..." );
   BenchmarkIOSvc ioService{
      conditionService,
      rawConditionIDs
   };

   // For each raw condition, add a pass-through algorithm which forwards the data without touching it
   examples_shared::print( "Setting up pass-through ConditionAlgs..." );
   using DerivationAlg = PassthroughAlg< ConditionType >;
   const auto derivationAlgs = examples_shared::setupDerivationMap< DerivationAlg >(
      conditionService,
      scheduler,
      rawConditionIDs,
      derivedConditionIDs
   );

   // Complete the condition processing pipeline with a readout stage
   examples_shared::print( "Setting up condition readout..." );
   const auto readHandles = examples_shared::setupConditionReaders< ConditionType >(
      conditionService,
      derivedConditionIDs
   );

   // Report the framework's view of the condition dataflow
   const auto dataflowMetadata = transientStore.getConditionDataflow();
   examples_shared::printDataflowMetadata( dataflowMetadata );
   
   // Run the event loop simulation
   examples_shared::print( "Running the simulated event loop..." );
   timer.start();
   scheduler.simulateEventLoop( eventBatch );
   const auto eventLoopDuration = timer.elapsed();

   // Display the simulation details
   const auto totalMiliseconds = eventLoopDuration.count();
   const int microsecondsPerEvent = float(totalMiliseconds) * 1000 / eventCount;
   examples_shared::print( "Simulating "
                           + std::to_string( eventCount ) + " events took "
                           + std::to_string( totalMiliseconds ) + " ms (~"
                           + std::to_string( microsecondsPerEvent ) + " µs/event)" );
}
#endif


int main()
{
   examples_shared::title( "CONDITION-MAPPING EVENT LOOP BENCHMARK (IO-bound + \"mapping\" alg)" );

#ifndef ALLOW_IO_IN_ALGORITHMS

   benchmark( 1 );
   examples_shared::newline();
   benchmark( examples_shared::empty_loop_params::conditionCount );

#else

   examples_shared::print( "This example is only available when algorithm IO is disabled. Sorry." );

#endif

   examples_shared::exampleSeparator();
   
   return 0;
}
