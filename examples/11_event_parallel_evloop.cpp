#include <cstddef>
#include <thread>
#include <vector>

#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/ConditionDataSet.hpp"
#include "detail/ParallelEventScheduler.hpp"

#include "framework/ConditionDataflow.hpp"
#include "framework/identifiers.hpp"
#include "framework/timing.hpp"

#include "examples/examples_shared.hpp"
#include "examples/MockIOSvc.hpp"


// This is a variant of the minimal event loop example that uses a parallel event loop
int main()
{
   examples_shared::title( "PARALLEL EMPTY EVENT LOOP" );

   // Parameters of this usage example are defined here
   using ConditionType = std::string;
   using MockConditionDataSet = std::vector< ConditionData< ConditionType > >;
   const auto threadPoolSize = std::thread::hardware_concurrency();
   const std::size_t eventCount{ examples_shared::empty_loop_params::eventCount };
   const std::size_t conditionCount{ examples_shared::empty_loop_params::conditionCount };
   const std::size_t eventSlotAmount{ 2*threadPoolSize };
   const std::size_t conditionSlotAmount{ 2*threadPoolSize };
   const std::chrono::milliseconds ioLatency{ 0 };
   
   // The following objects will be used for benchmarking
   examples_shared::Timer timer;
   
   // Setup the condition infrastructure
   examples_shared::print( "Initializing the condition infrastructure..." );
   TransientConditionStorageSvc transientStore{ conditionSlotAmount };
   ConditionSvc conditionService{ transientStore };
   detail::ParallelEventScheduler scheduler{ conditionService, threadPoolSize, eventSlotAmount };

   // Prepare a batch of event timestamps for the scheduler to process
   examples_shared::print( "Generating an event batch..." );
   const auto eventBatch = examples_shared::generateEventBatch( eventCount );

   // Generate IDs for a bunch of raw conditions
   examples_shared::print( "Generating condition IDs..." );
   const auto rawConditionIDs = examples_shared::generateConditionIDs( conditionCount );

   // Prepare to simulate the IO of these conditions
   examples_shared::print( "Setting up an IO simulation..." );
   MockIOSvc ioService{
      conditionService,
      rawConditionIDs,
      ioLatency
   };
   ioService.setMultipleMockOutputs< ConditionType >(
      rawConditionIDs,
      MockConditionDataSet{
         { "UNLIMITED VALIDITY!!!", framework::TimeInterval::INFINITE_INTERVAL }
      }
   );

   // Complete the condition processing pipeline with a readout stage
   examples_shared::print( "Setting up condition readout..." );
   const auto readHandles = examples_shared::setupConditionReaders< ConditionType >(
      conditionService,
      rawConditionIDs
   );

   // Report the framework's view of the condition dataflow
   const auto dataflowMetadata = transientStore.getConditionDataflow();
   examples_shared::printDataflowMetadata( dataflowMetadata );
   
   // Run the event loop simulation
   examples_shared::print( "Running the simulated event loop..." );
   timer.start();
   scheduler.simulateEventLoop( eventBatch );
   const auto eventLoopDuration = timer.elapsed();

   // Display the simulation details
   const auto totalMiliseconds = eventLoopDuration.count();
   const int microsecondsPerEvent = float(totalMiliseconds) * 1000 / eventCount;
   examples_shared::print( "Simulating "
                           + std::to_string( eventCount ) + " events took "
                           + std::to_string( totalMiliseconds ) + " ms (~"
                           + std::to_string( microsecondsPerEvent ) + " µs/event)" );
   
   // Visually separate the output of this example from the next
   examples_shared::exampleSeparator();
   
   return 0;
}
