#include <cstddef>
#include <thread>

#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionSvc.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/ConditionDataSet.hpp"
#include "detail/ParallelEventScheduler.hpp"

#include "framework/ConditionDataflow.hpp"
#include "framework/identifiers.hpp"
#include "framework/timing.hpp"

#include "examples/CPUCruncher.hpp"
#include "examples/examples_shared.hpp"
#include "examples/MockIOSvc.hpp"


// This is a variant of the io_and_alg_evloop example that uses a parallel event loop and a CPUCruncher alg.
// It illustrates how condition processing work can easily be parallelized.
// Since it relies on ConditionTransformer, it requires algorithm-based IO to be disabled.
int main()
{
   examples_shared::title( "PARALLEL CONDITION PROCESSING" );

#ifndef ALLOW_IO_IN_ALGORITHMS

   // Parameters of this usage example are defined here
   using ConditionType = int;
   const auto threadAmount = std::thread::hardware_concurrency();
   const std::size_t eventCount{ 128 };
   const std::size_t conditionCount{ 16 };
   const std::size_t conditionSlotAmount{ 2*threadAmount };
   const std::size_t eventSlotAmount{ 2*threadAmount };
   const std::chrono::milliseconds ioLatency{ 0 };
   constexpr std::chrono::milliseconds derivationDuration{ 32 };
   
   // The following objects will be used for benchmarking
   examples_shared::Timer timer;
   
   // Setup the condition infrastructure
   examples_shared::print( "Initializing the condition infrastructure..." );
   TransientConditionStorageSvc transientStore{ conditionSlotAmount };
   ConditionSvc conditionService{ transientStore };
   detail::ParallelEventScheduler scheduler{ conditionService, threadAmount, eventSlotAmount };

   // Prepare a batch of event timestamps for the scheduler to process
   examples_shared::print( "Generating an event batch..." );
   const auto eventBatch = examples_shared::generateEventBatch( eventCount );

   // Prepare a condition batch where conditions change on every event
   examples_shared::print( "Generating a condition batch..." );
   const auto conditionBatch = examples_shared::generatePerEventConditions( eventBatch );

   // Generate IDs for a bunch of raw conditions and associated derived conditions
   examples_shared::print( "Generating condition IDs..." );
   const auto rawConditionIDs = examples_shared::generateConditionIDs( conditionCount );
   const auto derivedConditionIDs = examples_shared::generateConditionIDs( conditionCount );

   // Prepare to simulate the IO of these conditions
   examples_shared::print( "Setting up an IO simulation..." );
   MockIOSvc ioService{
      conditionService,
      rawConditionIDs,
      ioLatency
   };
   ioService.setMultipleMockOutputs< ConditionType >(
      rawConditionIDs,
      conditionBatch
   );

   // For each raw condition, add a CPUCruncher algorithm which forwards the data after some busy waiting
   examples_shared::print( "Setting up CPU-crunching ConditionAlgs..." );
   using DerivationAlg = CPUCruncher< ConditionType, derivationDuration.count() >;
   const auto derivationAlgs = examples_shared::setupDerivationMap< DerivationAlg >(
      conditionService,
      scheduler,
      rawConditionIDs,
      derivedConditionIDs
   );

   // Complete the condition processing pipeline with a readout stage
   examples_shared::print( "Setting up condition readout..." );
   const auto readHandles = examples_shared::setupConditionReaders< ConditionType >(
      conditionService,
      derivedConditionIDs
   );

   // Report the framework's view of the condition dataflow
   const auto dataflowMetadata = transientStore.getConditionDataflow();
   examples_shared::printDataflowMetadata( dataflowMetadata );
   
   // Run the event loop simulation
   examples_shared::print( "Running the simulated event loop..." );
   timer.start();
   scheduler.simulateEventLoop( eventBatch );
   const auto eventLoopDuration = timer.elapsed();

   // Display the simulation details
   const auto totalMiliseconds = eventLoopDuration.count();
   const int microsecondsPerEvent = float(totalMiliseconds) * 1000 / eventCount;
   examples_shared::print( "Simulating "
                           + std::to_string( eventCount ) + " events took "
                           + std::to_string( totalMiliseconds ) + " ms (~"
                           + std::to_string( microsecondsPerEvent ) + " µs/event)" );

#else

   examples_shared::print( "This example is only available when algorithm IO is disabled. Sorry." );

#endif

   // Visually separate the output of this example from the next
   examples_shared::exampleSeparator();
   
   return 0;
}
