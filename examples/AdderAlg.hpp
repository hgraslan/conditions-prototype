#ifndef ADDER_ALG_HPP
#define ADDER_ALG_HPP

#include "ConditionTransformer.hpp"


// This example algorithm relies on ConditionTransformer, and is thus only available if Algorithm IO is disabled
#ifndef ALLOW_IO_IN_ALGORITHMS

// The algorithm takes two inputs ( x, y ) and returns their generalized sum ( x + y )
template< typename T >
class AdderAlg : public ConditionTransformer< T( T, T ) >
{
public:

   // Reuse ConditionTransformer's constructor as-is
   using Base = ConditionTransformer< T( T, T ) >;
   using Base::Base;

   // This is where the algorithm's core logic is implemented
   T operator()( const T & left,
                 const T & right ) const final override;

};

// Inline implementations
#include "examples/AdderAlg.ipp"

#endif

#endif
