template< typename T >
T
AdderAlg<T>::operator()( const T & left,
                         const T & right ) const
{
   return left + right;
}
