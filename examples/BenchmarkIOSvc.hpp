#ifndef BENCHMARK_IO_SVC_HPP
#define BENCHMARK_IO_SVC_HPP

#include "cpp_next/future.hpp"

#include "ConditionIOSvcBase.hpp"
#include "ConditionSlot.hpp"

#include "detail/executors.hpp"

#include "framework/identifiers.hpp"
#include "framework/timing.hpp"


// Forward declaration of other prototype components
class ConditionSvc;


// While the MockIOSvc is fine for general demonstration purposes, it actually has too high an overhead
// for the purpose of benchmarking the intrinsic overhead of IO services. So here is a specialization for
// the benchmark use case where there is one integer condition per event.
class BenchmarkIOSvc : public ConditionIOSvcBase
{
public:
   
   // Like any IO service, the mock condition IO service must be told where the condition service is,
   // and which conditions it is expected to produce.
   BenchmarkIOSvc(       ConditionSvc              & conditionService,
                   const framework::ConditionIDSet & expectedProducts,
                   const unsigned int                threadPoolSize = 1 );

   // As expected by IConditionIOSvc, there is a way to start the condition IO simulation asynchronously
   cpp_next::future<void> startConditionIO( const framework::TimePoint   & eventTimestamp,
                                            const ConditionSlotIteration & targetSlot ) final override;


private:
   
   // These are the handles we'll write data into
   using ProductHandle = ConditionWriteHandle< int >;
   std::vector< ProductHandle > m_products;

   // The IO will be run on the following thread pool
   detail::ThreadPoolExecutor m_executor;

};


// Inline implementations
#include "BenchmarkIOSvc.ipp"

#endif
