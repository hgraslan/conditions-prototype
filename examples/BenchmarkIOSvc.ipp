#include <thread>

#include "detail/single_use_bind.hpp"


inline
BenchmarkIOSvc::BenchmarkIOSvc(       ConditionSvc              & conditionService,
                                const framework::ConditionIDSet & expectedProducts,
                                const unsigned int                threadPoolSize )
   : ConditionIOSvcBase{ conditionService, expectedProducts }
   , m_executor{ threadPoolSize }
{
   m_products.reserve( expectedProducts.size() );
   for( const auto & productID : expectedProducts )
   {
      m_products.emplace_back( ConditionIOSvcBase::registerOutput<int>( productID ) );
   }
}


inline
cpp_next::future<void>
BenchmarkIOSvc::startConditionIO( const framework::TimePoint   & eventTimestamp,
                                  const ConditionSlotIteration & targetSlotIteration )
{
   // Make sure that all condition producers have been registered
   if( !ConditionIOSvcBase::conditionOutputsRegistered() ) throw CannotProduceCondition{};
   
   // DEBUG: For now, we ignore the list of missing raw conditions from the ConditionSvc.
   //        In the future, we may use this information to improve efficiency.
   const ConditionSlot & targetSlot = targetSlotIteration.slot;

   // Find out which condition producers must be run because the associated output is missing
   using ProductRefList = std::vector< std::reference_wrapper< const ProductHandle > >;
   ProductRefList missingProducts;
   for( const auto & product : m_products )
   {
      if( !product.hasValue( targetSlot ) ) {
         missingProducts.emplace_back( product );
      }
   }

   // If all output conditions are ready, exit immediately
   if( missingProducts.empty() ) return cpp_next::make_ready_future();
   
   // Otherwise, we will simulate condition IO as follows
   auto ioSimulationImpl = [this, eventTimestamp]( ConditionSlot  && targetSlot,
                                                   ProductRefList && missingProducts )
   {
      for( const auto & productRef : missingProducts )
      {
         productRef.get().put( targetSlot, { int(eventTimestamp), { eventTimestamp, eventTimestamp } } );
      }
   };

   // The condition IO simulator above takes a non-owning reference to the condition slot.
   // In C++14, we'll do this directly in the lambda by using move capture and a mutable operator().
   auto ioSimulation = detail::single_use_bind( std::move(ioSimulationImpl),
                                                targetSlot.ref(),
                                                std::move(missingProducts) );
   
   // Run the simulated condition IO asynchronously
   //
   // DEBUG: The C++ Concurrency TS currently does not provide a way to bound the amount of threads
   //        used by std::async, so we need to use a nonstandard Boost extension here, namely
   //        thread pool executors.
   //
   return cpp_next::async( m_executor, std::move(ioSimulation) );
}
