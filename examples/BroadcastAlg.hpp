#ifndef BROADCAST_ALG_HPP
#define BROADCAST_ALG_HPP

#include "ConditionMultiTransformer.hpp"


// This example algorithm relies on ConditionMultiTransformer, and is thus only available if Algorithm IO is disabled
#ifndef ALLOW_IO_IN_ALGORITHMS

// The algorithm simply feeds back its input into two outputs
template< typename T >
class BroadcastAlg : public ConditionMultiTransformer< std::tuple<T,T>( T ) >
{
public:

   // Get some useful definitions from our base class
   using Base = ConditionMultiTransformer< std::tuple<T,T>( T ) >;

   // Reuse ConditionMultiTransformer's constructor as-is
   using Base::Base;

   // This is where the algorithm's core logic is implemented
   std::tuple< T, T > operator()( const T & arg ) const final override;

};


// Inline implementations
#include "examples/BroadcastAlg.ipp"

#endif

#endif
