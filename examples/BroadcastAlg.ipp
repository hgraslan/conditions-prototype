template< typename T >
std::tuple< T, T >
BroadcastAlg<T>::operator()( const T & arg ) const
{
   return std::make_tuple( arg, arg );
}
