#ifndef CPU_CRUNCHER_HPP
#define CPU_CRUNCHER_HPP

#include "ConditionTransformer.hpp"

#include "examples/examples_shared.hpp"


// This example algorithm relies on ConditionTransformer, and is thus only available if Algorithm IO is disabled
#ifndef ALLOW_IO_IN_ALGORITHMS

// The algorithm feeds back its input as an output after keeping the CPU active for a while
template<
   typename T,
   int duration // Time during which the CPU should be kept busy, in milliseconds
>
class CPUCruncher : public ConditionTransformer< T( T ) >
{
public:

   // Reuse ConditionTransformer's constructor as-is
   using Base = ConditionTransformer< T( T ) >;
   using Base::Base;

   // This is where the algorithm's core logic is implemented
   T operator()( const T & arg ) const final override;

};


// Inline implementations
#include "examples/CPUCruncher.ipp"

#endif

#endif
