template<
   typename T,
   int duration
>
T
CPUCruncher<T, duration>::operator()( const T & arg ) const
{
   examples_shared::Timer timer;
   timer.start();
   do {
      ; // Wait for the expected duration, eating CPU time
   } while( timer.elapsed().count() < duration );
   return arg;
}
