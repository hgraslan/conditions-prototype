#ifndef MOCK_IO_SVC_HPP
#define MOCK_IO_SVC_HPP

#include <chrono>
#include <vector>

#include "cpp_next/future.hpp"

#include "ConditionIOSvcBase.hpp"
#include "ConditionSlot.hpp"

#include "detail/ConditionDataSet.hpp"
#include "detail/MockConditionProducer.hpp"
#include "detail/executors.hpp"

#include "framework/identifiers.hpp"
#include "framework/timing.hpp"


// Forward declaration of other prototype components
class ConditionSvc;


// This mock implementation of a condition IO service simulates I/O by loading data from a mock dataset with some
// arbitrary time delay. It can be used to test the latency hiding characteristics of the proposed infrastructure.
class MockIOSvc : public ConditionIOSvcBase
{
public:
   
   // Like any IO service, the mock condition IO service must be told where the condition service is,
   // and which conditions it is expected to produce. In addition, for the purpose of IO simulation,
   // it should have an estimate of the response time of the IO device that it simulates.
   MockIOSvc(       ConditionSvc              & conditionService,
              const framework::ConditionIDSet & expectedProducts,
              const std::chrono::milliseconds & ioLatency,
              const unsigned int                threadPoolSize = 1 );

   // The MockIOSvc does not carry out actual IO, but only simulates it. It should thus be
   // fed with a mock condition dataset to operate on.
   template< typename T >
   void setMockOutput( const framework::ConditionID      & productID,
                       const detail::ConditionDataSet<T> & output );

   // In example and benchmarking code, it is also often convenient to use the same dataset for several outputs
   template< typename T >
   void setMultipleMockOutputs( const framework::ConditionIDSet   & productIDs,
                                const detail::ConditionDataSet<T> & commonOutput );

   // As expected by IConditionIOSvc, there is a way to start the condition IO simulation asynchronously
   cpp_next::future<void> startConditionIO( const framework::TimePoint   & eventTimestamp,
                                            const ConditionSlotIteration & targetSlot ) final override;


private:
   
   // Internally, the IO of each individual condition is simulated by a MockConditionProducer
   std::vector< detail::MockConditionProducer > m_producers;
   
   // The simulated IO latency will be stored here
   const std::chrono::milliseconds m_ioLatency;

   // The IO will be run on the following thread pool
   detail::ThreadPoolExecutor m_executor;

};


// Inline implementations
#include "MockIOSvc.ipp"

#endif
