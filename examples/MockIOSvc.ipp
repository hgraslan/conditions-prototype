#include <thread>

#include "detail/single_use_bind.hpp"


inline
MockIOSvc::MockIOSvc(       ConditionSvc              & conditionService,
                      const framework::ConditionIDSet & expectedProducts,
                      const std::chrono::milliseconds & ioLatency,
                      const unsigned int                threadPoolSize )
   : ConditionIOSvcBase{ conditionService, expectedProducts }
   , m_ioLatency{ ioLatency }
   , m_executor{ threadPoolSize }
{ }


template< typename T >
void
MockIOSvc::setMockOutput( const framework::ConditionID      & productID,
                          const detail::ConditionDataSet<T> & output )
{
   
   // Register a write handle to the proposed condition
   auto writeHandle = ConditionIOSvcBase::registerOutput<T>( productID );
   
   // Set up a MockConditionProducer using this write handle and the user-specified condition dataset
   m_producers.emplace_back( std::move(writeHandle), output );
}


template< typename T >
void
MockIOSvc::setMultipleMockOutputs( const framework::ConditionIDSet   & productIDs,
                                   const detail::ConditionDataSet<T> & commonOutput )
{
   for( const auto & id : productIDs ) {
      setMockOutput<T>(
         id,
         commonOutput
      );
   }
}


inline
cpp_next::future<void>
MockIOSvc::startConditionIO( const framework::TimePoint   & eventTimestamp,
                             const ConditionSlotIteration & targetSlotIteration )
{
   // Make sure that all condition producers have been registered
   if( !ConditionIOSvcBase::conditionOutputsRegistered() ) throw CannotProduceCondition{};
   
   // DEBUG: For now, we ignore the list of missing raw conditions from the ConditionSvc.
   //        In the future, we may use this information to improve efficiency.
   const ConditionSlot & targetSlot = targetSlotIteration.slot;

   // Find out which condition producers must be run (because the associated output is missing)
   using ScheduledProducerList = std::vector< std::reference_wrapper< const detail::MockConditionProducer > >;
   ScheduledProducerList scheduledProducers;
   for( const auto & producer : m_producers )
   {
      if( producer.outputMissing( targetSlot ) ) {
         scheduledProducers.emplace_back( producer );
      }
   }

   // If all output conditions are ready, exit immediately
   if( scheduledProducers.empty() ) return cpp_next::make_ready_future();
   
   // Otherwise, we will simulate condition IO as follows
   auto ioSimulationImpl = [this, eventTimestamp]( ConditionSlot         && targetSlot,
                                                   ScheduledProducerList && scheduledProducers )
   {
      if( m_ioLatency.count() > 0 ) std::this_thread::sleep_for( m_ioLatency );
      for( const auto & producerRef : scheduledProducers )
      {
         producerRef.get().produceOutput( eventTimestamp, targetSlot );
      }
   };

   // The condition IO simulator above takes a non-owning reference to the condition slot.
   // In C++14, we'll do this directly in the lambda by using move capture and a mutable operator().
   auto ioSimulation = detail::single_use_bind( std::move(ioSimulationImpl),
                                                targetSlot.ref(),
                                                std::move(scheduledProducers) );
   
   // Run the simulated condition IO asynchronously
   //
   // DEBUG: The C++ Concurrency TS currently does not provide a way to bound the amount of threads
   //        used by std::async, so we need to use a nonstandard Boost extension here, namely
   //        thread pool executors.
   //
   return cpp_next::async( m_executor, std::move(ioSimulation) );
}
