#ifndef PASSTHROUGH_ALG_HPP
#define PASSTHROUGH_ALG_HPP

#include "ConditionTransformer.hpp"


// This example algorithm relies on ConditionTransformer, and is thus only available if Algorithm IO is disabled
#ifndef ALLOW_IO_IN_ALGORITHMS

// The algorithm simply feeds back its input as an output
template< typename T >
class PassthroughAlg : public ConditionTransformer< T( T ) >
{
public:

   // Reuse ConditionTransformer's constructor as-is
   using Base = ConditionTransformer< T( T ) >;
   using Base::Base;

   // This is where the algorithm's core logic is implemented
   T operator()( const T & arg ) const final override;

};


// Inline implementations
#include "examples/PassthroughAlg.ipp"

#endif

#endif
