template< typename T >
T
PassthroughAlg<T>::operator()( const T & arg ) const
{
   return arg;
}
