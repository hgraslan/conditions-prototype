#ifndef EXAMPLES_SHARED_HPP
#define EXAMPLES_SHARED_HPP

#include <chrono>
#include <cstddef>
#include <string>
#include <vector>

#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionSlot.hpp"
#include "ConditionSvc.hpp"

#include "detail/ConditionDataSet.hpp"

#include "framework/ConditionDataflow.hpp"
#include "framework/IScheduler.hpp"
#include "framework/identifiers.hpp"


// This header factors out some commonalities between the example programs
namespace examples_shared
{

// === FREE FUNCTIONS ===

// --- Assertion checking ---

// This function plays a similar role to assert(). Its input should be guaranteed to be true by the prototype.
// If it is false, it indicates a bug, so we call fail() to abort the example.
void check( const bool predicate );

// This function should be called when an assertion has failed. It throws PrototypeBugDetected.
void fail();

// --- Console display ---

// Give this example a nicely formatted title
void title( const std::string & text );

// Print a line of example log with fancy formatting
void print( const std::string & text );

// Display the condition dataflow metadata of the prototype framework in a human-readable way
void printDataflowMetadata( const framework::ConditionDataflow::Metadata & metadata,
                            const std::size_t                              maxConditions = 9 );

// Display the contents and IoV of a condition data blob
template< typename T >
void printCondition( const ConditionData<T> & data );

// Pretty-print the missing conditions metadata of an ConditionSlot iteration
void printMissingConditions( const framework::TimePoint   & eventTimestamp,
                             const ConditionSlotIteration & slotIteration );

// Skip a line in the example output log
void newline();

// Separate two examples in the output log
void exampleSeparator();

// --- Test data generators ---

// Generate a condition ID
framework::ConditionID generateConditionID();

// Generate a set of condition IDs
framework::ConditionIDSet generateConditionIDs( std::size_t amount );

// Generate a condition user ID
framework::ConditionUserID generateConditionUserID();

// Generate a batch of event timestamps for the Scheduler to process
using EventBatch = framework::IScheduler::EventBatch;
EventBatch generateEventBatch( std::size_t amount );

// Generate an integer condition data set which changes on every event in an event batch
detail::ConditionDataSet< int > generatePerEventConditions( const EventBatch & eventBatch );

// --- Event loop helpers ---

// Simulation parameters for the empty loops used to study minimal event scheduling latencies
namespace empty_loop_params
{
   const std::size_t eventCount     = 10000;
   const std::size_t conditionCount = 10000;
}

// Register read handles for a set of conditions of homogeneous type
template< typename T >
std::vector< ConditionReadHandle<T> >
setupConditionReaders(       ConditionSvc              & conditionService,
                       const framework::ConditionIDSet & conditionIDs );

// Given a functional algorithm class with an unary signature T(U), with T and U some types, set up a
// "map" derivation workflow where each input condition is transformed into a corresponding output.
template< typename UnaryAlg >
std::vector< std::unique_ptr< UnaryAlg > >
setupDerivationMap(       ConditionSvc              & conditionService,
                          framework::IScheduler     & scheduler,
                    const framework::ConditionIDSet & inputConditionIDs,
                    const framework::ConditionIDSet & outputConditionIDs );

// Given a functional algorithm class with a binary signature T(U,U), with T and U some types, set up a
// "merge" derivation workflow where pairs of inputs are transformed into a corresponding output.
template< typename BinaryAlg >
std::vector< std::unique_ptr< BinaryAlg > >
setupDerivationMerge(       ConditionSvc              & conditionService,
                            framework::IScheduler     & scheduler,
                      const framework::ConditionIDSet & inputConditionIDs,
                      const framework::ConditionIDSet & outputConditionIDs );

// Given a functional algorithm class with a binary signature {T,T}(U), with T and U some types, set up a
// "broadcast" derivation workflow where inputs are transformed into a corresponding pair of outputs.
template< typename BroadcastAlg >
std::vector< std::unique_ptr< BroadcastAlg > >
setupDerivationBroadcast(       ConditionSvc              & conditionService,
                                framework::IScheduler     & scheduler,
                          const framework::ConditionIDSet & inputConditionIDs,
                          const framework::ConditionIDSet & outputConditionIDs );


// === SIMPLE PERFORMANCE TIMER ===

class Timer
{
public:

   // Durations will be reported in integer milliseconds
   using Duration = std::chrono::milliseconds;

   // Run this method to start the timer
   void start();

   // Run this method to measure the elapsed time since the timer was started
   Duration elapsed() const;


private:

   using Clock = std::chrono::steady_clock;
   using TimePoint = Clock::time_point;
   TimePoint m_start;

};

}


// Inline implementations
#include "examples/examples_shared.ipp"

#endif
