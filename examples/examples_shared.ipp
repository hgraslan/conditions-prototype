#include <algorithm>
#include <iostream>
#include <numeric>

#include "exceptions.hpp"

#include "framework/ConditionUsage.hpp"


namespace examples_shared
{

inline
void
check( const bool predicate )
{
   if( !predicate ) fail();
}


inline
void
fail()
{
   throw PrototypeBugDetected{};
}


inline
void
title( const std::string & text )
{
   std::cout << "=== " << text << " ===" << std::endl;
   newline();
}


inline
void
print( const std::string & text )
{
   std::cout << "* " << text << std::endl;
}


inline
void
printDataflowMetadata( const framework::ConditionDataflow::Metadata & metadata,
                       const std::size_t                              maxConditions )
{
   // Display metadata on at most "maxConditions" conditions
   print( "Here is the framework's view of the condition dataflow:" );
   std::size_t conditionsSoFar = 0;
   for( const auto & mapIterator : metadata )
   {
      // State the metadata we have about one condition
      const framework::ConditionUsage & usage = mapIterator.second;
      std::cout << "   - Condition " << mapIterator.first << " is produced by user ";
      std::cout << *usage.producer() << " and consumed by users { ";
      for( const auto & reader : usage.consumers() ) {
         std::cout << reader << ' ';
      }
      std::cout << '}' << std::endl;

      // Bound the amount of conditions we can display in many-condition examples
      if( ++conditionsSoFar == maxConditions ) break;
   }
   
   // If we did not describe all conditions, notify the user
   if( metadata.size() > maxConditions ) {
      std::cout << "   (..."
                << std::to_string( metadata.size() - maxConditions ) << " more conditions...)"
                << std::endl;
   }
}


template< typename T >
void
printCondition( const ConditionData<T> & data )
{
   std::cout << "   - Value: " << data.value << std::endl;
   std::cout << "   - IoV: [" << data.iov.start() << ", " << data.iov.end() << ']' << std::endl;
}


inline
void
printMissingConditions( const framework::TimePoint   & eventTimestamp,
                        const ConditionSlotIteration & slotIteration )
{
   const auto & missingConditions = slotIteration.missingConditions;

   bool rawConditionsMissing = !missingConditions.raw.empty();
   bool derivedConditionsMissing = !missingConditions.derived.empty();
   bool conditionsMissing = ( rawConditionsMissing || derivedConditionsMissing );

   std::string header = "Condition slot for timestamp " + std::to_string( eventTimestamp )
                           + (conditionsMissing ? " currently lacks the following conditions:"
                                                : " is completely filled" );
   print( header );

   if( rawConditionsMissing ) {
      std::cout << "   - Raw: { ";
      for( const auto & id : missingConditions.raw ) { std::cout << id << ' '; }
      std::cout << '}' << std::endl;
   }
   
   if( derivedConditionsMissing ) {
      std::cout << "   - Derived: { ";
      for( const auto & id : missingConditions.derived ) { std::cout << id << ' '; }
      std::cout << '}' << std::endl;
   }
}


inline
void
newline()
{
   std::cout << std::endl;
}


inline
void
exampleSeparator()
{
   newline();
   newline();
}


inline
framework::ConditionID
generateConditionID()
{
   static framework::ConditionID currentID = 0;
   return ++currentID;
}


inline
framework::ConditionIDSet
generateConditionIDs( std::size_t amount )
{
   framework::ConditionIDSet ids( amount );
   std::generate_n(
      std::inserter( ids, ids.end() ),
      amount,
      generateConditionID
   );
   return ids;
}


inline
framework::ConditionUserID
generateConditionUserID()
{
   static framework::ConditionUserID currentID = 0;
   return ++currentID;
}


inline
framework::IScheduler::EventBatch
generateEventBatch( std::size_t amount )
{
   framework::IScheduler::EventBatch eventBatch( amount );
   std::iota( eventBatch.begin(), eventBatch.end(), 0 );
   return eventBatch;
}


inline
detail::ConditionDataSet< int >
generatePerEventConditions( const EventBatch & eventBatch )
{
   std::vector< ConditionData< int > > conditionBatch;
   conditionBatch.reserve( eventBatch.size() );
   std::transform(
      eventBatch.cbegin(),
      eventBatch.cend(),
      std::back_inserter( conditionBatch ),
      []( const framework::TimePoint & time ) -> ConditionData< int > {
         return { time, framework::TimeInterval{ time, time } };
      }
   );
   return detail::ConditionDataSet< int >{ conditionBatch };
}


template< typename T >
std::vector< ConditionReadHandle<T> >
setupConditionReaders(       ConditionSvc              & conditionService,
                       const framework::ConditionIDSet & conditionIDs )
{
   // Prepare to register ConditionReadHandles
   static const framework::ConditionUserID readerID{ generateConditionUserID() };
   auto & transientStore = conditionService.transientStore();
   
   // Register one read handle per input condition
   std::vector< ConditionReadHandle<T> > readHandles;
   readHandles.reserve( conditionIDs.size() );
   for( const auto & id : conditionIDs ) {
      readHandles.emplace_back(
         transientStore.registerInput<T>(
            readerID,
            id
         )
      );
   }
   
   // Return all those handles to the caller
   return readHandles;
}


template< typename UnaryAlg >
std::vector< std::unique_ptr< UnaryAlg > >
setupDerivationMap(       ConditionSvc              & conditionService,
                          framework::IScheduler     & scheduler,
                    const framework::ConditionIDSet & inputConditionIDs,
                    const framework::ConditionIDSet & outputConditionIDs )
{
   // Make sure that the amount of input and output conditions match
   check( outputConditionIDs.size() == inputConditionIDs.size() );
   
   // Register one unary algorithm per input condition
   std::vector< std::unique_ptr< UnaryAlg > > algs;
   algs.reserve( inputConditionIDs.size() );
   for(
      auto inputConditionIt = inputConditionIDs.cbegin(), outputConditionIt = outputConditionIDs.cbegin();
      inputConditionIt != inputConditionIDs.cend();
      ++inputConditionIt, ++outputConditionIt
   ) {
      algs.emplace_back(
         new UnaryAlg{
            conditionService,
            scheduler,
            *outputConditionIt,
            { *inputConditionIt }
         }
      );
   }

   // Return these algorithm instances to the caller
   return algs;
}


template< typename BinaryAlg >
std::vector< std::unique_ptr< BinaryAlg > >
setupDerivationMerge(       ConditionSvc              & conditionService,
                            framework::IScheduler     & scheduler,
                      const framework::ConditionIDSet & inputConditionIDs,
                      const framework::ConditionIDSet & outputConditionIDs )
{
   // Make sure that the amount of input and output conditions match
   check( ( inputConditionIDs.size() % 2 == 0 ) &&
          ( outputConditionIDs.size() == inputConditionIDs.size()/2 ) );
   
   // Register one merge algorithm per pair of input conditions
   std::vector< std::unique_ptr< BinaryAlg > > algs;
   algs.reserve( outputConditionIDs.size() );
   for(
      auto inputConditionIt = inputConditionIDs.cbegin(), outputConditionIt = outputConditionIDs.cbegin();
      inputConditionIt != inputConditionIDs.cend();
      ++inputConditionIt, ++outputConditionIt
   ) {
      const auto & leftInputCondition = *inputConditionIt;
      ++inputConditionIt;
      const auto & rightInputCondition = *inputConditionIt;
      algs.emplace_back(
         new BinaryAlg{
            conditionService,
            scheduler,
            *outputConditionIt,
            { leftInputCondition, rightInputCondition }
         }
      );
   }

   // Return these algorithm instances to the caller
   return algs;
}




template< typename BroadcastAlg >
std::vector< std::unique_ptr< BroadcastAlg > >
setupDerivationBroadcast(       ConditionSvc              & conditionService,
                                framework::IScheduler     & scheduler,
                          const framework::ConditionIDSet & inputConditionIDs,
                          const framework::ConditionIDSet & outputConditionIDs )
{
   // Make sure that the amount of input and output conditions match
   check( ( outputConditionIDs.size() % 2 == 0 ) &&
          ( inputConditionIDs.size() == outputConditionIDs.size()/2 ) );
   
   // Register one broadcast algorithm per input condition
   std::vector< std::unique_ptr< BroadcastAlg > > algs;
   algs.reserve( inputConditionIDs.size() );
   for(
      auto inputConditionIt = inputConditionIDs.cbegin(), outputConditionIt = outputConditionIDs.cbegin();
      inputConditionIt != inputConditionIDs.cend();
      ++inputConditionIt, ++outputConditionIt
   ) {
      const auto & leftOutputCondition = *outputConditionIt;
      ++outputConditionIt;
      const auto & rightOutputCondition = *outputConditionIt;
      algs.emplace_back(
         new BroadcastAlg{
            conditionService,
            scheduler,
            { leftOutputCondition, rightOutputCondition },
            { *inputConditionIt }
         }
      );
   }

   // Return these algorithm instances to the caller
   return algs;
}


inline
void
Timer::start()
{
   m_start = Clock::now();
}


inline
Timer::Duration
Timer::elapsed() const
{
   const TimePoint end = Clock::now();
   const Duration elapsed = std::chrono::duration_cast<Duration>( end - m_start );
   return elapsed;
}

}
