// DEBUG : Include every header here in order to trigger header compilation errors

#include "cpp_next/any.hpp"
#include "cpp_next/future.hpp"
#include "cpp_next/optional.hpp"
#include "cpp_next/utility.hpp"

#include "ConditionAlgBase.hpp"
#include "ConditionData.hpp"
#include "ConditionHandle.hpp"
#include "ConditionIOSvcBase.hpp"
#include "ConditionKind.hpp"
#include "ConditionMultiTransformer.hpp"
#include "ConditionSlot.hpp"
#include "ConditionSvc.hpp"
#include "ConditionTransformer.hpp"
#include "exceptions.hpp"
#include "MissingConditions.hpp"
#include "TransientConditionStorageSvc.hpp"

#include "detail/AnyConditionData.hpp"
#include "detail/AnyConditionHandle.hpp"
#include "detail/AnyConditionHandleImpl.hpp"
#include "detail/ConditionDataSet.hpp"
#include "detail/ConditionHandleImpl.hpp"
#include "detail/ConditionSlotID.hpp"
#include "detail/ConditionSlotKnowledge.hpp"
#include "detail/ConditionSlotPromises.hpp"
#include "detail/ConditionStore.hpp"
#include "detail/ConditionStoreInternals.hpp"
#include "detail/ConditionTransformerBase.hpp"
#include "detail/dynamic_array.hpp"
#include "detail/executors.hpp"
#include "detail/MockConditionProducer.hpp"
#include "detail/OptionalConditionKind.hpp"
#include "detail/ParallelEventScheduler.hpp"
#include "detail/run_statement_pack.hpp"
#include "detail/SchedulerBase.hpp"
#include "detail/SequentialScheduler.hpp"
#include "detail/SharedConditionData.hpp"
#include "detail/single_use_bind.hpp"
#include "detail/SingleUseBindWrapper.hpp"
#include "detail/when_all.hpp"
#include "detail/when_all_impl.hpp"
#include "detail/when_any.hpp"
#include "detail/when_any_impl.hpp"

#include "framework/ConditionDataflow.hpp"
#include "framework/ConditionUsage.hpp"
#include "framework/IConditionAlg.hpp"
#include "framework/IConditionIOSvc.hpp"
#include "framework/IScheduler.hpp"
#include "framework/identifiers.hpp"
#include "framework/timing.hpp"

#include "examples/AdderAlg.hpp"
#include "examples/BenchmarkIOSvc.hpp"
#include "examples/BroadcastAlg.hpp"
#include "examples/CPUCruncher.hpp"
#include "examples/examples_shared.hpp"
#include "examples/MockIOSvc.hpp"
#include "examples/PassthroughAlg.hpp"


int main()
{
   // DEBUG: Usage examples originate in main.cpp, and are subsequently moved to a dedicated
   //        code unit once they become feature-complete.
   
   return 0;
}
