#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

#include <exception>


// All exceptions thrown by this prototype should inherit from this class.
class ConditionPrototypeException : public std::exception { };

// Errors which may only originate from prototype bugs (and not usage errors) will be reported as follows
class PrototypeBugDetected : public ConditionPrototypeException { };

// If there is a need for an unimplemented code stub, it should throw this exception
class NotImplementedYet : public PrototypeBugDetected { };

// Condition type checking errors will be reported as follows
class InconsistentConditionType : public ConditionPrototypeException { };

// Attempts to register two writers for a single condition will be reported as follows
class ProducerAlreadyRegistered : public ConditionPrototypeException { };

#endif
