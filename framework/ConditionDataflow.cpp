#include "framework/ConditionDataflow.hpp"


namespace framework
{

void
ConditionDataflow::check() const
{
   // For each condition...
   for( const auto & mapIterator : m_metadata )
   {
      // ...check the usage pattern
      const framework::ConditionUsage & usage = mapIterator.second;

      // A condition must have a producer
      const auto & producer = usage.producer();
      if( !producer ) throw ConditionLacksProducer{};

      // A condition should have at least one consumer
      const auto & consumers = usage.consumers();
      if( consumers.empty() ) throw ConditionLacksConsumers{};
   }
}


ConditionDataflow::Metadata
ConditionDataflow::report() const
{
   check();
   return m_metadata;
}

}
