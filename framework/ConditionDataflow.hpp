#ifndef CONDITION_DATAFLOW_HPP
#define CONDITION_DATAFLOW_HPP

#include <unordered_map>

#include "exceptions.hpp"

#include "framework/ConditionUsage.hpp"
#include "framework/identifiers.hpp"


namespace framework
{

// Condition data flow metadata summarizes all the framework-collected knowledge about condition usage patterns
class ConditionDataflow
{
public:

   // Take note that a framework component is consuming a certain condition, and thinks it has a certain type
   template< typename T >
   void registerConsumer( const ConditionUserID & consumerID,
                          const ConditionID     & targetID );

   // Take note that a framework component is producing a certain condition, and thinks it has a certain type
   template< typename T >
   void registerProducer( const ConditionUserID & producerID,
                          const ConditionID     & targetID );

   // Perform extra consistency checks under the assumption that dataflow collection is now complete
   void check() const;
   class ConditionLacksProducer : public ConditionPrototypeException { };
   class ConditionLacksConsumers : public ConditionPrototypeException { };

   // Report the collected dataflow metadata (currently implies a check())
   using Metadata = std::unordered_map< framework::ConditionID, ConditionUsage >;
   Metadata report() const;


private:

   // Register a (possibly new) condition, and get access to its usage data
   template< typename T >
   ConditionUsage & registerCondition( const ConditionID & id );

   // Internally, we are currently storing the metadata exactly as reported
   Metadata m_metadata;

};

}


// Inline implementations
#include "framework/ConditionDataflow.ipp"

#endif
