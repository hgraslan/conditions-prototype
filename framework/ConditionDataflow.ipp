namespace framework
{

template< typename T >
void
ConditionDataflow::registerConsumer( const framework::ConditionUserID & clientID,
                                     const framework::ConditionID     & targetID )
{
   // Get the usage record for this condition
   auto & usage = registerCondition<T>( targetID );
   
   // Mark the client as a reader of this condition
   usage.addConsumer( clientID );
}


template< typename T >
void
ConditionDataflow::registerProducer( const framework::ConditionUserID & clientID,
                                     const framework::ConditionID     & targetID )
{
   // Get the usage record for this condition
   auto & usage = registerCondition<T>( targetID );
   
   // Mark the client as the writer of this condition
   usage.setProducer( clientID );
}


template< typename T >
ConditionUsage &
ConditionDataflow::registerCondition( const framework::ConditionID & id )
{
   // Make sure we have a usage record for this condition
   auto emplaceResult = m_metadata.emplace( id, framework::ConditionUsage{} );
   auto & usage = emplaceResult.first->second;
   
   // If this is the first time we refer to this condition, set its type, otherwise check it
   if( emplaceResult.second ) {
      usage.setConditionType<T>();
   } else {
      usage.checkConditionType<T>();
   }
   
   // Provide access to the usage record for further modification
   return usage;
}

}
