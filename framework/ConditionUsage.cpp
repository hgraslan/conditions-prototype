#include "exceptions.hpp"

#include "framework/ConditionUsage.hpp"


namespace framework
{

void
ConditionUsage::setProducer( const framework::ConditionUserID & producerID )
{
   if( m_producer ) throw ProducerAlreadyRegistered{};
   m_producer = producerID;
}


cpp_next::optional< framework::ConditionUserID >
ConditionUsage::producer() const
{
   return m_producer;
}


void
ConditionUsage::addConsumer( const framework::ConditionUserID & consumerID )
{
   m_consumers.insert( consumerID );
}


framework::ConditionUserIDSet
ConditionUsage::consumers() const
{
   return m_consumers;
}

}
