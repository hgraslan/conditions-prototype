#ifndef CONDITION_USAGE_HPP
#define CONDITION_USAGE_HPP

#include <cstddef>

#include "cpp_next/optional.hpp"

#include "framework/identifiers.hpp"


namespace framework
{

// This class describes the usage pattern of a single condition:
//    - What is the type of data being manipulated?
//    - Who produces the data?
//    - Who consumes the data?
//
class ConditionUsage
{
public:

   // Ideally, we should set the condition type at initialization time. Unfortunately, a C++ template
   // constructor syntax limitation prevents us to do that. The next best thing is thus to set the
   // condition type manually right after initializing a ConditionUsage object.
   template< typename T >
   void setConditionType();

   // After this is done, we can freely type-check further references to this condition.
   // Type-checking errors will be reported by throwing InconsistentConditionType.
   template< typename T >
   void checkConditionType() const;
   
   // A condition may only have one producer, which we can set anytime.
   // Duplicate producer registration will be reported by throwing ProducerAlreadyRegistered.
   void setProducer( const framework::ConditionUserID & producerID );
   
   // At any point in time, we can check whether a condition's producer has been set, and if so what it is.
   cpp_next::optional< framework::ConditionUserID > producer() const;
   
   // A condition can have multiple consumers, each possibly accessing it multiple times
   void addConsumer( const framework::ConditionUserID & consumerID );
   
   // The set of consumers is reported with duplicate removed
   framework::ConditionUserIDSet consumers() const;


private:

   cpp_next::optional< std::size_t > m_typeHash;
   cpp_next::optional< framework::ConditionUserID > m_producer;
   framework::ConditionUserIDSet m_consumers;

};

}


// Inline implementations
#include "framework/ConditionUsage.ipp"

#endif
