#include <typeinfo>

#include "exceptions.hpp"


namespace framework
{

template< typename T >
void
ConditionUsage::setConditionType()
{
   if( bool( m_typeHash ) ) throw PrototypeBugDetected{};
   m_typeHash = typeid(T).hash_code();
}


template< typename T >
void
ConditionUsage::checkConditionType() const
{
   if( !m_typeHash ) throw PrototypeBugDetected{};
   if( *m_typeHash != typeid(T).hash_code() ) throw InconsistentConditionType{};
}

}
