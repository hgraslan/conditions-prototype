#ifndef I_CONDITION_ALG_HPP
#define I_CONDITION_ALG_HPP

#include "ConditionSlot.hpp"

#include "framework/timing.hpp"


namespace framework
{

// The main purpose of condition algorithms is to compute derived conditions from the raw condition data that was
// fetched by condition IO services.
//
// For compatibility with the ATLAS condition infrastructure, we also allow condition algorithms to carry out condition
// IO themselves, given the caveat that an algorithm doing this can block and should thus be treated specially by the
// Gaudi scheduler, for example by being scheduled on a dedicated thread pool.
//
// The following minimal interface is used by the prototype in order to simulate condition algorithm processing. During
// Gaudi integration, the relevant functionality should be integrated into Gaudi and this interface should be dropped.
//
class IConditionAlg
{
public:

   // Like any interface class, IConditionAlg is default constructible...
   IConditionAlg() = default;
   
   // ...but condition algorithms are neither movable nor copyable
   IConditionAlg( const IConditionAlg & ) = delete;
   IConditionAlg( IConditionAlg && ) = delete;
   IConditionAlg & operator=( const IConditionAlg & ) = delete;
   IConditionAlg & operator=( IConditionAlg && ) = delete;

   // Tell whether a condition algorithm has all of its input dependencies satisfied and is thus ready to run
   virtual bool inputsPresent( const ConditionSlot & slot ) const = 0;

   // Tell whether a condition algorithm should be executed (because its output data is missing)
   virtual bool outputMissing( const ConditionSlot & slot ) const = 0;

   // Execute the condition processing algorithm. If algorithms solely perform condition derivation, access
   // to the condition slot is all they need. In order to perform IO, they will also need the event timestamp.
   virtual void execute( const ConditionSlot     & slot
#ifdef ALLOW_IO_IN_ALGORITHMS
                       , const framework::TimePoint & eventTimestamp
#endif
                       ) const = 0;

#ifdef ALLOW_IO_IN_ALGORITHMS
   // Tell whether a condition algorithm can perform condition IO and should thus be scheduled in a special way
   virtual bool performsConditionIO() const = 0;
#endif

};

}

#endif
