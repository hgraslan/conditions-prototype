#ifndef I_CONDITION_IO_SVC_HPP
#define I_CONDITION_IO_SVC_HPP

#include "cpp_next/future.hpp"

#include "ConditionSlot.hpp"

#include "framework/timing.hpp"


namespace framework
{

// Condition IO services are in charge of loading raw conditions from an IO source, such as a database of a file.
// Although the concrete implementation will depend on the nature of the IO source, they share a common interface.
class IConditionIOSvc
{
public:
   
   // Like any interface class, IConditionIOSvc is default constructible...
   IConditionIOSvc() = default;
   
   // ...but condition IO services are neither movable nor copyable
   IConditionIOSvc( const IConditionIOSvc & ) = delete;
   IConditionIOSvc( IConditionIOSvc && ) = delete;
   IConditionIOSvc & operator=( const IConditionIOSvc & ) = delete;
   IConditionIOSvc & operator=( IConditionIOSvc && ) = delete;

   // The ConditionSvc can start the IO managed by a service, then later synchronize with it using a future
   virtual cpp_next::future<void> startConditionIO( const framework::TimePoint   & eventTimestamp,
                                                    const ConditionSlotIteration & targetSlot ) = 0;

};

}

#endif
