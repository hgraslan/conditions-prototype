#ifndef I_SCHEDULER_HPP
#define I_SCHEDULER_HPP

#include <vector>

#include "exceptions.hpp"

#include "framework/IConditionAlg.hpp"
#include "framework/timing.hpp"


namespace framework
{

// For testing and benchmarking purposes, we have to simulate the Gaudi event loop.
// The Scheduler mocks that are used to this end will follow the following interface.
class IScheduler
{
public:

   // To be scheduled, ConditionAlgs must register to the scheduler during initialization
   virtual void registerConditionAlg( const IConditionAlg & alg ) = 0;

   // After initialization, the mock scheduler can carry out its event loop simulation.
   // If a scheduler stall is detected, it will be reported by throwing SchedulerStallDetected.
   using EventBatch = std::vector< framework::TimePoint >;
   class SchedulerStallDetected : public ConditionPrototypeException { };
   virtual void simulateEventLoop( const EventBatch & events ) = 0;

};

}

#endif
