The classes and structures that appear within this directory and namespace are
placeholders for implementation details of Gaudi. They are not a critical part
of this proposal, and will likely change greatly during framework integration.
