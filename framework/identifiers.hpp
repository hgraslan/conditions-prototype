#ifndef IDENTIFIERS_HPP
#define IDENTIFIERS_HPP

#include <cstdint>
#include <unordered_set>


namespace framework
{

// Identifier of a condition (time-dependent detector element), and collections thereof.
using ConditionID = int;
using ConditionIDSet = std::unordered_set< ConditionID >;

// A ConditionUserID is a unique identifier for a Gaudi entity (service, algorithm...) that uses conditions.
// Currently, such identifiers are simply implemented by casting the this pointer of the relevant component.
// We will also want to manipulate collections of condition user identifiers.
using ConditionUserID = std::uintptr_t;
using ConditionUserIDSet = std::unordered_set< ConditionUserID >;

}

#endif
