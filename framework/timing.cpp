#include <limits>

#include "framework/timing.hpp"


namespace framework
{

const TimeInterval TimeInterval::NULL_INTERVAL{ 1, 0 };

const TimeInterval TimeInterval::INFINITE_INTERVAL{ std::numeric_limits<TimePoint>::min(),
                                                    std::numeric_limits<TimePoint>::max() };

}
