#ifndef TIMING_HPP
#define TIMING_HPP

#include <initializer_list>


namespace framework
{

// Notion of experiment time
using TimePoint = int;


// Notion of time interval
class TimeInterval
{
public:
   
   // Construct a time interval from a pair of bounds, left- and right-inclusive
   TimeInterval( const TimePoint & start, const TimePoint & end );
   
   // The null time interval contains no time point
   static const TimeInterval NULL_INTERVAL;
   
   // The infinite time interval contains all time points
   static const TimeInterval INFINITE_INTERVAL;
   
   // Check the bounds of a time interval
   TimePoint start() const;
   TimePoint end() const;
   
   // Check whether a time interval is null
   bool isNull() const;
   
   // Check whether a time point falls into an interval
   bool contains( const TimePoint & t ) const;
   
   // Compute the intersection of time intervals in place
   void intersectWith( const TimeInterval & other );

   // Compute the intersection of a list of time intervals. As set theory would demand,
   // the intersection of an empty list of time intervals is the infinite interval.
   static TimeInterval intersect( std::initializer_list<TimeInterval> intervals );


private:

   TimePoint m_start, m_end;

};

}


// Inline implementations
#include "framework/timing.ipp"

#endif
