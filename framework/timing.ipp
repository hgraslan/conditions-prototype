#include <algorithm>


namespace framework
{

inline
TimeInterval::TimeInterval( const TimePoint & start, const TimePoint & end )
   : m_start{ start }
   , m_end{ end }
{ }


inline
TimePoint
TimeInterval::start() const
{
   return m_start;
}


inline
TimePoint
TimeInterval::end() const
{
   return m_end;
}


inline
bool
TimeInterval::isNull() const
{
   return m_end < m_start;
}


inline
bool
TimeInterval::contains( const TimePoint & t ) const
{
   return (t >= m_start) && (t <= m_end);
}


inline
void
TimeInterval::intersectWith( const TimeInterval & other )
{
   m_start = std::max( m_start, other.m_start );
   m_end = std::min( m_end, other.m_end );
}


inline
TimeInterval
TimeInterval::intersect( std::initializer_list<TimeInterval> intervals )
{
   TimeInterval result = INFINITE_INTERVAL;
   for( const auto & interval : intervals ) {
      result.intersectWith( interval );
   }
   return result;
}

}
